-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: blockchain
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bank_list`
--

DROP TABLE IF EXISTS `bank_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bank_list` (
  `code` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `parent_code` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bank_list`
--

LOCK TABLES `bank_list` WRITE;
/*!40000 ALTER TABLE `bank_list` DISABLE KEYS */;
INSERT INTO `bank_list` VALUES ('1201001','000000','Ngân hàng TMCP Công thương Việt Nam (Vietinbank)',1),('1201002','1201001','NH TMCPCT TP HANOI',1),('1201003','1201001','NH TMCPCT DONG DA',1),('1201004','1201001','NH TMCPCT BA DINH',1),('1201005','1201001','NH TMCPCT C.DUONG',1),('1201006','1201001','NH TMCPCT DONG ANH',1),('1201007','1201001','NH TMCPCT THANH XUAN',1),('1201008','1201001','NH TMCPCT NAM T.LONG',1),('1201009','1201001','NH TMCPCT BAC HN',1),('1201010','1201001','NH TMCPCT DONG HANOI',1),('1201011','1201001','NH TMCPCT H.KIEM',1),('1201012','1201001','NH TMCPCT HAIBATRUNG',1),('1201013','1201001','NH TMCPCT TAY HANOI',1),('1201014','1201001','NH TMCPCT HOANG MAI',1),('1201015','1201001','NH TMCPCT SONG NHUE',1),('1201016','1201001','NH TMCPCT QUANG TRUNG',1),('1202001','000000','Ngân hàng TMCP Đầu tư và Phát triển Việt Nam (BIDV)',1),('1202002','1202001','BIDV - Chi nhánh Sở giao dịch 1',1),('1202003','1202001','BIDV - Chi nhánh Hà Nội',1),('1202004','1202001','BIDV - Chi nhánh Bắc Hà Nội',1),('1202005','1202001','BIDV - Chi nhánh Hà Thành',1),('1202006','1202001','BIDV - Chi nhánh Thăng Long',1),('1202007','1202001','BIDV - Chi nhánh Đông Đô',1),('1202008','1202001','BIDV - Chi nhánh Đông Hà Nội',1),('1202009','1202001','BIDV - Chi nhánh Quang Trung',1),('1202010','1202001','BIDV - Chi nhánh Cầu Giấy',1),('1202011','1202001','BIDV - Chi nhánh Hai Bà Trưng',1),('1202012','1202001','BIDV - Chi nhánh Thành Đô',1),('1203001','000000','Ngân hàng TMCP Ngoại thương Việt Nam (Vietcombank)',1),('1203002','1203001','SGD Vietcombank',1),('1203003','1203001','Vietcombank Ha Noi',1),('1203004','1203001','Vietcombank Thang Long',1),('1203005','1203001','Vietcombank Ba Dinh',1),('1203006','1203001','Vietcombank Thanh Cong',1),('1203007','1203001','Vietcombank Chuong Duong',1),('1203008','1203001','Vietcombank Hoan Kiem',1),('1203009','1203001','Vietcombank Ha Tay',1),('1203010','1203001','Vietcombank Thanh Xuan',1),('1203011','1203001','Vietcombank Tây Hồ',1),('1203012','1203001','Vietcombank Sóc Sơn',1),('1203013','1203001','Vietcombank Đông Anh',1);
/*!40000 ALTER TABLE `bank_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_list`
--

DROP TABLE IF EXISTS `company_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_list` (
  `code` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `budget_code` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_list`
--

LOCK TABLES `company_list` WRITE;
/*!40000 ALTER TABLE `company_list` DISABLE KEYS */;
INSERT INTO `company_list` VALUES ('100696937','Công ty TNHH Hoa nam','754',1),('100697659','Công Ty TNHH Sơn Hà','754',1),('100697930','Công Ty Trách Nhiệm Hữu Hạn Phong Nam','754',1),('100698638','Công ty TNHH Hoa việt','554',1),('100698941','Công ty TNHH Trần Liên Thịnh','554',1),('100700929','Công ty TNHH thương mại Đông Đô','754',1),('100701506','Công Ty TNHH Ngân Hạnh','754',1),('100702813','Công Ty Cổ Phần Mặt Trời Vàng','554',1),('100703863','Công ty cổ phần sản xuất dịch vụ xuất nhập khẩu từ liêm','554',1),('100703863001','CN Cty Cổ Phần Sản Xuất Dịch Vụ Xuất Nhập Khẩu Từ Liêm','558',1),('100705187','Công Ty Cổ Phần Phát Triển Tây Hà Nội','558',1),('100774832001','Chi Nhánh Cty TNHH Europ Continents Việt Nam Tại Thành Phố Hồ Chí Minh','151',1),('100774864','Công ty SEASAFICO   Hà nội','151',1),('100774871','Công ty TNHH sao Hương','754',1),('100774896','Công ty TNHH năng lượng viễn thông','754',1),('100774913','Doanh Nghiệp Tư Nhân Cơ Khí Việt Nhật','555',1),('100774920','Công Ty TNHH  Hoàng Vũ','554',1),('100775000','Công ty TNHH Cát Sơn','554',1),('100775018','Công ty TNHH thương mại Hương Thuỷ','554',1),('100775025','Công ty TNHH Phương Long','754',1);
/*!40000 ALTER TABLE `company_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customs_unit`
--

DROP TABLE IF EXISTS `customs_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customs_unit` (
  `parent_code` varchar(45) CHARACTER SET utf8 NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `code` varchar(45) CHARACTER SET utf8 NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customs_unit`
--

LOCK TABLES `customs_unit` WRITE;
/*!40000 ALTER TABLE `customs_unit` DISABLE KEYS */;
INSERT INTO `customs_unit` VALUES ('0','Cục Hải quan Hà Nội','1',1),('1','Chi cục HQ cửa khẩu SBQT Nội Bài','01AB',1),('1','Chi cục HQ Gia Lâm','01AC',1),('1','HQ Sân bay Nội Bài - Đội Nhập','01AZ',1),('1','Chi cục HQ SB Nội Bài - Đội thủ tục hàng hóa XNK','01B1',1),('1','Chi cục HQ Sân bay Nội bài-Đội CPN','01B2',1),('1','Chi cục HQ Sân bay Nội bài-Đội HH Nhập','01B3',1),('1','Chi cục HQ Sân bay Nội bài-Đội QuanLyThue','01B4',1),('1','Chi cục HQ SB Nội Bài - Đội hành lý XC','01B5',1),('1','Chi cục HQ SB Nội Bài - Đội hành lý NC','01B6',1),('1','Chi cục HQ Sân bay Nội bài-Đội Giám Sát','01B7',1),('1','Chi cục HQ Tuyên Quang','01BB',1),('1','CC HQ Yên Bái','01BT',1),('1','HQ Mỹ Đình (thuộc HQ Bưu điện Hà Nội)','01D1',1),('1','Chi cục HQ Bưu Điện HN - FEDEX','01D2',1),('1','Chi cục HQ Bưu Điện HN - UPS','01D3',1),('1','Chi Cục HQ Chuyển phát nhanh','01DD',1),('1','Chi cục HQ Gia Thuỵ','01IK',1),('0','Cục Hải quan TP Hồ Chí Minh','2',1),('2','Chi cục HQ Sân bay quốc tế Tân Sơn Nhất','02B1',1),('2','CC HQ SB Tân Sơn Nhất - SCSC','02B4',1),('2','Chi cục HQ CK Cảng Sài Gòn khu vực II','02CC',1),('2','Chi cục HQ khu vực 1 (Cát lái)','02CI',1),('2','HQ Cảng Nhà Rồng','02CM',1),('2','HQ Cảng Nhà Bè','02CN',1),('2','CC HQ CK Cảng Hiệp Phước (HCM)','02CV',1),('2','CC HQ CK Tân Cảng (Hồ Chí Minh)','02CX',1),('2','CC HQ Chuyển phát nhanh','02DS',1),('2','Chi cục HQ KCX Linh Trung (Hồ Chí Minh)','02F1',1),('2','HQ KCX Linh Trung II (Hồ Chí Minh)','02F2',1),('2','Chi cục Hải quan Khu Công nghệ cao','02F3',1),('2','Chi cục HQ CK Cảng Sài Gòn khu vực III','02H1',1),('2','Đội thủ tục và Giám sát xăng dầu XNK','02H2',1),('2','HQ Cảng Vict','02H3',1),('2','Chi cục HQ Cảng Sài gòn khu vực IV (ICD2','02IJ',1),('2','Chi cục HQ CK Cảng Sài Gòn khu vực IV','02IK',1),('2','Hải Quan Transimex - ICD','02IV',1),('2','Chi cục HQ Tân tạo','02NR',1),('2','Phòng Giám Quản II HQ TP HCM','02PA',1),('2','Chi cục HQ quản lý hàng đầu tư HCM','02PG',1),('2','Chi cục HQ quản lý hàng gia công HCM','02PJ',1),('0','Cục Hải quan TP Hải phòng','3',1),('3','Hải quan Sân bay Cát Bi','03AB',1),('3','Chi cục HQ CK cảng HP KV I','03CC',1),('3','Chi cục HQ Thái Bình','03CD',1),('3','Chi cục HQ CK cảng HP KV II','03CE',1),('3','Chi cục HQ Bưu Điện TP Hải Phòng','03DF',1),('3','Chi cục HQ cửa khẩu cảng Đình Vũ','03EE',1),('3','Điểm Kiểm tra Sóng Thần (Tân Hoàn Cầu)','03KG',1),('3','Hải quan Kho Ngoại quan HP','03KH',1),('3','Chi cục HQ KCX và KCN Hải Phòng','03NK',1),('3','Chi cục HQ quản lý hàng ĐT-GC','03PA',1),('3','Phòng TM CBL và XL HQ TP Hải Phòng','03PB',1),('3','Phòng KTS thông quan (HP)','03PD',1),('3','Chi cục HQ Hải dương','03PJ',1),('3','Chi cục HQ Hưng yên','03PL',1),('3','Phòng kiểm tra sau thông quan - HQHP','03PQ',1),('3','Hải quan Hải dương (Thuộc HQ Hải Phòng)','03PX',1),('3','Phòng QLRR Hải Phòng','03RR',1),('3','Chi cục HQ CK cảng HP KV III','03TG',1),('3','Đội Kiểm soát HQ Hải Phòng','03VA',1),('3','HQ KCX Hải Phòng','03XI',1);
/*!40000 ALTER TABLE `customs_unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaration_status`
--

DROP TABLE IF EXISTS `declaration_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaration_status` (
  `code` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaration_status`
--

LOCK TABLES `declaration_status` WRITE;
/*!40000 ALTER TABLE `declaration_status` DISABLE KEYS */;
INSERT INTO `declaration_status` VALUES ('1','Tạo mới TBT/thông tin thuế của tờ khai',1),('2','Đang yêu cầu thanh toán',1),('3','Đã thanh toán, chờ xác nhận HTNVT',1),('4','Đã hoàn thành NVT',1),('5','Đã hoạch toán kho bạc',1);
/*!40000 ALTER TABLE `declaration_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_type`
--

DROP TABLE IF EXISTS `tax_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_type` (
  `prefix` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `code` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax_type`
--

LOCK TABLES `tax_type` WRITE;
/*!40000 ALTER TABLE `tax_type` DISABLE KEYS */;
INSERT INTO `tax_type` VALUES ('XK','Xuất khẩu',1,'1'),('NK','Nhập khẩu',1,'2'),('VA','Giá trị gia tăng',1,'3'),('TD','Tiêu thụ đặc biệt',1,'4'),('TV','Tự vệ',1,'5'),('MT','Bảo vệ môi trường',1,'6'),('TC','Chống trợ cấp',1,'7'),('PD','Chống phân biệt đối xử',1,'8');
/*!40000 ALTER TABLE `tax_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `treasury_list`
--

DROP TABLE IF EXISTS `treasury_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `treasury_list` (
  `code` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `parent_code` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `treasury_list`
--

LOCK TABLES `treasury_list` WRITE;
/*!40000 ALTER TABLE `treasury_list` DISABLE KEYS */;
INSERT INTO `treasury_list` VALUES ('100000','Kho bạc Nhà nước - Hà Nội',1,'000000'),('100001','Kho bạc Nhà nước - Hà Nội - Quận Cầu Giấy',1,'100000'),('100002','Kho bạc Nhà nước - Hà Nội - Quận Hai Bà Trưng',1,'100000'),('200000','Kho bạc Nhà nước - TP Hồ Chí Minh',1,'000000'),('200001','Kho bạc Nhà nước - TP Hồ Chí Minh - Quận 1',1,'200000'),('200002','Kho bạc Nhà nước - TP Hồ Chí Minh - Quận 2',1,'200000'),('300000','Kho bạc Nhà nước - Hải Dương',1,'000000');
/*!40000 ALTER TABLE `treasury_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_code`
--

DROP TABLE IF EXISTS `type_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_code` (
  `code` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `prefix` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(2555) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_code`
--

LOCK TABLES `type_code` WRITE;
/*!40000 ALTER TABLE `type_code` DISABLE KEYS */;
INSERT INTO `type_code` VALUES ('A11','A11','Nhập kinh doanh tiêu dùng',1),('A12','A12','Nhập kinh doanh sản xuất',1),('A21','A21','Chuyển tiêu thụ nội địa từ nguồn tạm nhập',1),('A31','A31','Nhập hàng XKbị trả lại',1),('A41','A41','Nhập kinh doanh của doanh nghiệp đầu tư',1),('A42','A42','Chuyển tiêu thụ nội địa khác',1),('E11','E11','Nhập nguyên liệu của doanh nghiệp chế xuất',1),('E13','E13','Nhập tạo tài sản cố định của doanh nghiệp chế xuất',1),('E15','E15','Nhập nguyên liệu của doanh nghiệp chế xuấttừ nội địa ',1),('E21','E21','Nhập nguyên liệu để gia công cho thương nhân nước ngoài',1),('E23','E23','Nhập nguyên liệu gia công từ hợp đồng khác chuyển sang',1),('E31','E31','Nhập nguyên liệu sản xuất xuất khẩu',1),('E41','E41','Nhập sản phẩm thuê gia công ở nước ngoài',1),('G11','G11','Tạm nhập hàng kinh doanh tạm nhập tái xuất',1),('G12','G12','Tạm nhập máy móc, thiết bị phục vụ dự án có thời hạn',1),('G13','G13','Tạm nhập miễn thuế',1),('G14','G14','Tạm nhập khác',1),('G51','G51','Tái nhập hàng đã tạm xuất',1),('C11','C11','Hàng gửi kho ngoại quan',1),('C21','C21','Hàng đưa vào Khu phi thuế quan',1),('AEO','AEO','Doanh nghiệp AEO',1),('H11','H11','Loại khác',1),('B11','B11','Xuất kinh doanh, XKcủa doanh nghiệp đầu tư',1),('B12','B12','Xuất sau khi đã tạm xuất',1),('B13','B13','Xuất trả hàng đã nhập khẩu',1),('E42','E42','Xuất sản phẩm của Doanh nghiệp chế xuất',1),('E46','E46','Hàng của Doanh nghiệp chế xuấtvào nội địa để GC',1),('E52','E52','Xuất sản phẩm GC cho thương nhân nước ngoài',1),('E54','E54','Xuất nguyên liệu gia công từ hợp đồng khác sang',1),('E56','E56','Xuất sản phẩm gia công giao hàng tại nội địa',1),('E62','E62','Xuất sản phẩm Sản xuất xuất khẩu',1),('E82','E82','Xuất nguyên liệu, vật tư thuê gia công ở nước ngoài',1),('G21','G21','Tái xuất hàng kinh doanh tạm nhập tái xuất',1),('G22','G22','Tái xuất máy móc, thiết bị phục vụ dự án có thời hạn',1),('G23','G23','Tái xuất miễn thuế hàng tạm nhập',1),('G24','G24','Tái xuất khác',1),('G61','G61','Tạm xuất hàng hóa',1),('C12','C12','Hàng xuất kho ngoại quan',1),('C22','C22','Hàng đưa ra Khu phi thuế quan',1),('AEO','AEO','Doanh nghiệp AEO',1),('H21','H21','Loại hình khác',1);
/*!40000 ALTER TABLE `type_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `username` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `role` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `channel` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `unit` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('doanhnghiep1','e10adc3949ba59abbe56e057f20f883e','doanhnghiep1@doanhnghiep.vn',1,'company','mychannel','100696937'),('haiquan1','e10adc3949ba59abbe56e057f20f883e','haiquan1@haiquan.vn',1,'customs','mychannel','01AB'),('haiquan2','e10adc3949ba59abbe56e057f20f883e','haiquan2@haiquan.vn',1,'customs','mychannel','1'),('nganhang1','e10adc3949ba59abbe56e057f20f883e','nganhang1@nganhang.vn',1,'bank','mychannel','1201001'),('nganhang2','e10adc3949ba59abbe56e057f20f883e','nganhang2@nganhang.vn',1,'bank','mychannel','1201001');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `voucher_type`
--

DROP TABLE IF EXISTS `voucher_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voucher_type` (
  `code` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `voucher_type`
--

LOCK TABLES `voucher_type` WRITE;
/*!40000 ALTER TABLE `voucher_type` DISABLE KEYS */;
INSERT INTO `voucher_type` VALUES ('tax_declaration','TBT/thông tin thuế của tờ khai',1),('payment_request','Yêu cầu thanh toán',1),('payment_confirm_reject','Xác nhận thanh toán',1),('tax_mission_confirm','Xác nhận hoàn thành nghĩa vụ thuế',1),('treasury_accounting','Hoạch toán kho bạc',1);
/*!40000 ALTER TABLE `voucher_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-22 19:03:18
