//SPDX-License-Identifier: Apache-2.0
// nodejs server setup
const router = require("express").Router();
// call the packages we need
// const path = require("path");
// const fs = require("fs");
// const os = require("os");


const controllerManager = require("./controller-mgr");
// const { catchErrors } = require("./helpers/errorHandlers");
// const userEndpoint = require("./apis/userEndpoint");
const { getAuthorize } = require("./middlewares/authMiddleware");
const config = controllerManager.getConfig();
const crypto = require("./helpers/cryptHelpers.js");

const log4js = require("log4js");
const logger = log4js.getLogger();

const controller_db = require("./controller-db");

router.get("/viewca", function(req, res) {
  const user = req.query.user || config.user;
  const controller = controllerManager.getInstance(
    user,
    req.query.channel || "mychannel"
  );

  // each method require different certificate of user
  const ret = controller.viewca(user);
  res.send(ret);
});

router.get("/query", function(req, res) {
  logger.info("query");
  const user = req.query.user || config.user;
  const controller = controllerManager.getInstance(
    user,
    req.query.channel || "mychannel"
  );
  const request = {
    //targets : --- letting this default to the peers assigned to the channel
    chaincodeId: req.query.chaincode,
    fcn: req.query.method,
    args: req.query.arguments || []
  };

  // each method require different certificate of user
  controller
    .query(user, request)
    .then(ret => {
      res.send(ret.toString());
    })
    .catch(err => {
      res.status(500).send(err);
    });
});

router.get("/invoke", function(req, res) {
  const user = req.query.user || config.user;
  const controller = controllerManager.getInstance(
    user,
    req.query.channel || "mychannel"
  );
  const request = {
    chaincodeId: req.query.chaincode,
    fcn: req.query.method,
    args: req.query.arguments || []
  };
  // each method require different certificate of user
  controller
    .invoke(req.query.user || config.user, request)
    .then(ret => {
      res.json(ret);
    })
    .catch(err => {
      console.log(err);
      res.status(500).send(err);
    });
});

// Protected routes
// router.get("/users", catchErrors(userEndpoint.getUsers));
// router.get("/get-user-current", catchErrors(userEndpoint.getUserCurrent));
router.post("/signup", function(req, res) {
  controller_db.signup(req)
    .then(result => {
      const username = req.body.username;
      const password = crypto.md5(req.body.password);
      const channel = req.body.channel;
      const role = req.body.role;
      const controller = controllerManager.getInstance(
        username || config.user,
        channel || "mychannel"
      );
      controller.signup(username, password, role)
      .then(ret => {
        var code = ret.code;
        if(code == 0) {
          controller_db.deleleUser(username)
            .then(re => {
              res.send({"code":0, "message":re});
            })
            .catch(ex => {
              res.send({"code":0, "message":ex.original.sqlMessage});
            })
        } else {
          res.send(ret);
        }
      })
    })
    .catch(err => {
      console.log("signup error", err);
      res.send({"code":0, "message":err.original.sqlMessage});
    });
});

router.post("/signin", function(req, res) {
  const username = req.body.username;
  const password = req.body.password;
  controller_db.signin(username, password)
    .then(result => {
      if(result.length > 0) {
        res.send({"code":1, "message":result});
      } else {
        res.send({"code":0, "message":"Sai tài khoản hoặc mật khẩu! Vui lòng kiểm tra lại!"});
      }
    })
    .catch(err => {
      console.log("signin error: ", err);
      res.send({"code":0, "message":err.original.sqlMessage});
    });
});

router.get("/userinfo", function(req, res) {
  const username = req.query.username;
  controller_db.getUserInfo(username)
    .then(result => {
      res.send(result);
    })
    .catch(err => {
      res.send({"code":0,"message":err.original.sqlMessage});
    });
});

router.get("/choicelist", function(req, res) {
  const choicelistType = req.query.choicelistType;
  controller_db.choicelist(choicelistType)
    .then(result => {
      res.send(result);
    })
    .catch(err => {
      res.send({"code":0,"message":err.original.sqlMessage});
    });
});

router.get("/test", function(req, res) {
  return res.status(200).json({"error": "error"});

  // // TODO:
  // const io = req.app.get('socketio');

  // io.sockets.on("connection", function(socket) {
  //   // console.log('Connection to client established');
  //   // console.log("Connection", socket.connected);
  
  //   // Send message to Client
  //   socket.emit("announcements", { message: "A new user has joined!" });
  
  //   // Receive message from Client
  //   socket.on("event", function(data) {
  //     console.log("A client sent us this dumb message:", data.message);
  //   });
  // });
});

//router.use(getAuthorize);

module.exports = router;
