const crypto = require("./helpers/cryptHelpers.js");
const Sequelize = require('sequelize');

var sequelize = new Sequelize('blockchain', 'root', '123456', {
	host: 'localhost',
	dialect: 'mysql',
	pool: {
		max: 5,
		min: 0,
		idle: 10000
	},
});

module.exports = {

    choicelist : function name(choicelistType) {
        var sql = "SELECT * FROM "+choicelistType+" WHERE status = 1";
        return sequelize.query(sql, {type: sequelize.QueryTypes.SELECT})
            .then(result => {
                return result;
            })
            .catch(err => {
                return err;
            });
    },

    signin : function(username, password) {
        var pass = crypto.md5(password);
        var sql =   "SELECT username, role, channel, unit "+
                    "FROM users "+
                    "WHERE username = '" + username + "' AND password = '" + pass + "' AND status = 1 "+
                    "LIMIT 1";
        return sequelize.query(sql, {type: sequelize.QueryTypes.SELECT})
            .then(result => {
                return result;
            })
            .catch(err => {
                throw err;
            });
    },

    signup : function(request) {
        var username = request.body.username;
        var password = crypto.md5(request.body.password);
        var email = request.body.email;
        var role = request.body.role;
        var channel = request.body.channel;
        var unit = request.body.unit;
        var sql =   "INSERT INTO users "+
                    "VALUES ('"+username+"','"+password+"','"+email+"', 1, '"+role+"', '"+channel+"', '"+unit+"')";
        return sequelize.query(sql, {type: sequelize.QueryTypes.INSERT})
            .then(result => {
                return result;
            })
            .catch(err => {
                throw err;
            });
    },

    setUserStatus : function(username, status) {
        var sql = "UPDATE users SET status = "+status+" WHERE username = '"+username+"'";
        return sequelize.query(sql, {type: sequelize.QueryTypes.UPDATE})
            .then(result => {
                return result;
            })
            .catch(err => {
                throw err;
            });
    },

    deleleUser : function(username) {
        var sql = "DELETE FROM users WHERE username = '"+username+"'";
        return sequelize.query(sql, {type: sequelize.QueryTypes.DELETE})
            .then(result => {
                return result;
            })
            .catch(err => {
                throw err;
            });
    },

    getUserInfo : function(username) {
        var sql =   "SELECT username, role, channel, unit "+
                    "FROM users "+
                    "WHERE username = '" + username + "' AND status = 1 "+
                    "LIMIT 1";
        return sequelize.query(sql, {type: sequelize.QueryTypes.SELECT})
            .then(result => {
                return result;
            })
            .catch(err => {
                throw err;
            });
    }
};