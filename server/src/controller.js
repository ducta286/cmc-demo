/*
* Copyright IBM Corp All Rights Reserved
*
* SPDX-License-Identifier: Apache-2.0
*/
/*
 * Chaincode query
 */
// var fs = require("fs-extra");
const x509 = require('x509');
const Fabric_Client = require('fabric-client');

const Fabric_Utils = require('fabric-client/lib/utils.js');
const path = require('path');
const util = require('util');
const fs = require('fs');
const os = require('os');
// const moment = require("moment");
const User = require('fabric-client/lib/User.js');
const CaService = require('fabric-ca-client/lib/FabricCAClientImpl.js');
const log4js = require("log4js");
const logger = log4js.getLogger();

module.exports = function(config) {
  const fabric_client = new Fabric_Client();
  const channel = fabric_client.newChannel(config.channelName);
  const grpcProtocol = config.tlsEnabled ? 'grpcs://' : 'grpc://';
  const peerConfig = config.tlsEnabled
    ? {
        pem: config.peerPem,
        'ssl-target-name-override':
          config.peerDomain || config.peerHost.split(':')[0]
      }
    : null;
  const peer = fabric_client.newPeer(
    grpcProtocol + config.peerHost,
    peerConfig
  );

  const ordererConfig = config.tlsEnabled
    ? {
        pem: config.ordererPem,
        'ssl-target-name-override':
          config.ordererDomain || config.ordererHost.split(':')[0]
      }
    : null;

  const orderer = fabric_client.newOrderer(
    grpcProtocol + config.ordererHost,
    ordererConfig
  );

  const store_path =
    process.env.KEY_STORE_PATH || path.join(__dirname, '../hfc-key-store');

  channel.addPeer(peer);
  channel.addOrderer(orderer);

  console.log('Peer: ' + grpcProtocol + config.peerHost);
  console.log('Store path:' + store_path);

  // var logFile = process.env.NAMESPACE + '.' + config.channelName + '.csv'
  // console.log('logFile', logFile)
  let currentSubmitter = null;
  const instance = {
    viewca(user) {
      var obj = JSON.parse(fs.readFileSync(store_path + '/' + user, 'utf8'));
      var cert = x509.parseCert(obj.enrollment.identity.certificate);
      return cert;
    },

    get_member_user(user) {
      // console.log(user);
      if (currentSubmitter) {
        // console.log("has found");
        return Promise.resolve(currentSubmitter);
      }

      // Fabric_Utils.setConfigSetting(
      //   "key-value-store",
      //   "fabric-client/lib/impl/CouchDBKeyValueStore.js"
      // );

      // const keyvalueStoreConfig = {
      //   name: "mychannel",
      //   url: "http://localhost:5984"
      // };

      const keyvalueStoreConfig = {
        path: store_path
      };

      return Fabric_Client.newDefaultKeyValueStore(keyvalueStoreConfig)
        .then(store => {
          fabric_client.setStateStore(store);
          const crypto_suite = Fabric_Client.newCryptoSuite();
          // use the same location for the state store (where the users' certificate are kept)
          // and the crypto store (where the users' keys are kept)
          const crypto_store = Fabric_Client.newCryptoKeyStore(
            keyvalueStoreConfig
          );
          crypto_suite.setCryptoKeyStore(crypto_store);
          fabric_client.setCryptoSuite(crypto_suite);
          return fabric_client.getUserContext(user, true);
        })
        .then(submitter => {
          if (submitter && submitter.isEnrolled()) {
            // console.log(submitter);
            currentSubmitter = submitter;
            return submitter;
          } else {
            throw new Error('failed');
          }
        })
        .catch(err => {
          console.error('[err]', err);
        });
    },

    getEventTxPromise(eventAdress, transaction_id_string) {
      // by default, orderer using batch timer about 2 seconds, after that it will do the batch cut and broadcast
      // to commiting peers
      return new Promise((resolve, reject) => {
        let event_hub = fabric_client.newEventHub();
        event_hub.setPeerAddr('grpc://' + eventAdress);
        console.log('eventhub: grpc://' + eventAdress);

        let handle = setTimeout(() => {
          event_hub.unregisterTxEvent(transaction_id_string);
          resolve(null); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
        }, 150000);
        // register everything
        event_hub.connect();
        event_hub.registerTxEvent(
          transaction_id_string,
          (event_tx_id, status) => { //(event_tx_id, status)
            console.log("event_tx_id: ", event_tx_id);
            console.log("status: ", status)
            clearTimeout(handle);
            //channel_event_hub.unregisterTxEvent(event_tx_id); let the default do this
            console.log('Successfully received the transaction event');
            event_hub.unregisterTxEvent(transaction_id_string);
            resolve(transaction_id_string);
          },
          error => {
            reject(
              new Error('There was a problem with the eventhub ::' + error)
            );
          }
        );
      });
    },

    query(user, request) {
      return this.get_member_user(user)
        .then(user_from_store => {
          return channel.queryByChaincode(request);
        })
        .then(query_responses => {
          // query_responses could have more than one  results if there multiple peers were used as targets
          if (query_responses && query_responses.length == 1) {
            if (query_responses[0] instanceof Error) {
              throw query_responses[0];
            } else {
              return query_responses[0];
            }
          } else {
            console.log('No payloads were returned from query');
            return null;
          }
        });
    },

    invoke(user, invokeRequest) {
      var tx_id;

      return this.get_member_user(user)
        .then(user_from_store => {
          tx_id = fabric_client.newTransactionID();

          console.log('invokeRequest:', invokeRequest);

          return channel.sendTransactionProposal({
            chaincodeId: invokeRequest.chaincodeId,
            fcn: invokeRequest.fcn,
            args: invokeRequest.args,
            txId: tx_id
          });
        })
        .then(results => {
          var proposalResponses = results[0];
          var proposal = results[1];

          if (
            proposalResponses &&
            proposalResponses[0].response &&
            proposalResponses[0].response.status === 200
          ) {
            var transaction_id_string = tx_id.getTransactionID();
            const txPromise = this.getEventTxPromise(
              config.eventHost,
              transaction_id_string
            );

            const sendPromise = channel.sendTransaction({
              proposalResponses: proposalResponses,
              proposal: proposal
            });

            return Promise.all([sendPromise, txPromise]);
          } else {
            // console.log("CATCH ERROR SERVER PROPOSAL:", proposalResponses[0]);
            // throw proposalResponses[0];
            return proposalResponses[0];
          }
        })
        .catch(err => {
          // console.log("CATCH ERROR SERVER:", err)
          throw err;
        });
    },

    signup(username, password, role) {
      var Fabric_CA_Client = require("fabric-ca-client");
      var defaultConfig = require("./config");
      return Fabric_Client.newDefaultKeyValueStore({
        path: store_path
      })
      .then(state_store => {
        // assign the store to the fabric client
        fabric_client.setStateStore(state_store);
        var crypto_suite = Fabric_Client.newCryptoSuite();
        // use the same location for the state store (where the users' certificate are kept)
        // and the crypto store (where the users' keys are kept)
        var crypto_store = Fabric_Client.newCryptoKeyStore({ path: store_path });
        crypto_suite.setCryptoKeyStore(crypto_store);
        fabric_client.setCryptoSuite(crypto_suite);
        var tlsOptions = {
          trustedRoots: [],
          verify: false
        };
        // be sure to change the http to https when the CA is running TLS enabled
        fabric_ca_client = new Fabric_CA_Client(
          (defaultConfig.tlsEnabled ? "https://" : "http://") +
            defaultConfig.caServer,
          tlsOptions,
          null,
          crypto_suite
        );

        // first check to see if the admin is already enrolled
        return fabric_client.getUserContext("admin", true);
      })
      .then(user_from_store => {
        if (user_from_store && user_from_store.isEnrolled()) {
          console.log("Successfully loaded admin from persistence");
          admin_user = user_from_store;
        } else {
          throw new Error("Failed to get admin.... run registerAdmin.js");
        }

        // at this point we should have the admin user
        // first need to register the user with the CA server
        return fabric_ca_client.register(
          {
            enrollmentID: username,
            enrollmentSecret: password,
            affiliation: "org1.department1",
            attrs: [{ name: "role", value: role, ecert: true }]
          },
          admin_user
        );
      })
      .then(secret => {
        // next we need to enroll the user with CA server
        console.log("Successfully registered " + username + " - secret:" + secret);

        return fabric_ca_client.enroll({
          enrollmentID: username,
          enrollmentSecret: secret,
          attr_reqs: [{ name: "role", optional: false }]
        });
      })
      .then(enrollment => {
        console.log('Successfully enrolled member user "' + username + '" ');
        return fabric_client.createUser({
          username: username,
          mspid: "Org1MSP",
          cryptoContent: {
            privateKeyPEM: enrollment.key.toBytes(),
            signedCertPEM: enrollment.certificate
          }
        });
      })
      .then(user => {
        member_user = user;

        return fabric_client.setUserContext(member_user);
      })
      .then(() => {
        console.log(      
          username +
            " was successfully registered and enrolled and is ready to intreact with the fabric network"
        );
        logger.info(username +
            " was successfully registered and enrolled and is ready to intreact with the fabric network");
      
        return {"code":1,"message":"successfully"};
      })
      .catch(err => {
        logger.error("Failed to register: " + err);
        console.error("Failed to register: " + err);
        if (err.toString().indexOf("Authorization") > -1) {
          console.error(
            "Authorization failures may be caused by having admin credentials from a previous CA instance.\n" +
              "Try again after deleting the contents of the store directory " +
              store_path
          );
        }
        return {"code":0,"message":"Failed to register"};
      });
    }
  };

  return instance;
};
