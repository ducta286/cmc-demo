#!/bin/bash
#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#
# Exit on first error, print all commands.
set -ev

# Shut down the Docker containers that might be currently running.
docker-compose -f docker-compose.yml down

echo "Cleaning chaincode images and container..."
echo
# Delete docker containers
dockerContainers=$(docker ps -a --format '{{.ID}} {{.Names}}' | awk '$2~/^dev-peer/{print $1}')
if [ "$dockerContainers" != "" ]; then     
  docker rm -f $dockerContainers > /dev/null
fi

chaincodeImages=$(docker images --format '{{.ID}} {{.Repository}}' | awk '$2~/^dev-peer/{print $1}')  
if [ "$chaincodeImages" != "" ]; then     
  docker rmi $chaincodeImages > /dev/null
fi  