// Doanh nghiệp
export const navigationCompany = {
  items: [
    // {
    //   name: "Dashboard",
    //   url: "/dashboard",
    //   icon: "icon-speedometer",
    //   badge: {
    //     variant: "info",
    //     text: "NEW"
    //   }
    // },
    {
      name: "Danh sách tờ khai",
      url: "/base/congty",
      icon: "icon-calculator"
    },
    // {
    //   name: "Giấy nộp thuế",
    //   url: "/base/giaynopthue",
    //   icon: "icon-calculator"
    // },
    {
      name: "Tra cứu tờ khai",
      url: "/base/tracuutk",
      icon: "icon-calculator"
    }
    // {
    //   name: "Tờ khai thuế",
    //   url: "/base/declaration",
    //   icon: "icon-calculator"
    // },
    // {
    //   name: "Tra cứu tờ khai",
    //   url: "/base/tracuutk",
    //   icon: "icon-calculator"
    // },
    // {
    //   name: "Duyệt thanh toán",
    //   url: "/base/duyetthanhtoan",
    //   icon: "icon-calculator"
    // },
    // {
    //   name: "Xác nhận htvnt",
    //   url: "/base/xacnhanhtvnt",
    //   icon: "icon-calculator"
    // },
    // {
    //   name: "Xác nhận nộp thuế",
    //   url: "/base/xacnhannopthue",
    //   icon: "icon-calculator"
    // },
    // {
    //   divider: true
    // },
    // {
    //   title: true,
    //   name: "Extras"
    // },
    // {
    //   name: "Pages",
    //   url: "/pages",
    //   icon: "icon-star",
    //   children: [
    //     {
    //       name: "Login",
    //       url: "/login",
    //       icon: "icon-star"
    //     },
    //     {
    //       name: "Register",
    //       url: "/register",
    //       icon: "icon-star"
    //     },
    //     {
    //       name: "Error 404",
    //       url: "/404",
    //       icon: "icon-star"
    //     },
    //     {
    //       name: "Error 500",
    //       url: "/500",
    //       icon: "icon-star"
    //     }
    //   ]
    // }
  ]
};

// Ngân hàng
export const navigationBank = {
  items: [
    // {
    //   name: "Dashboard",
    //   url: "/dashboard",
    //   icon: "icon-speedometer",
    //   badge: {
    //     variant: "info",
    //     text: "NEW"
    //   }
    // },
    {
      name: "Danh sách yêu cầu thanh toán",
      url: "/base/nganhang",
      icon: "icon-calculator"
    },
    // {
    //   name: "Duyệt thanh toán",
    //   url: "/base/duyetthanhtoan",
    //   icon: "icon-calculator"
    // },
    {
      name: "Tra cứu tờ khai",
      url: "/base/tracuutk",
      icon: "icon-calculator"
    }
  ]
};

// Hải quan
export const navigationCustoms = {
  items: [
    // {
    //   name: "Dashboard",
    //   url: "/dashboard",
    //   icon: "icon-speedometer",
    //   badge: {
    //     variant: "info",
    //     text: "NEW"
    //   }
    // },
    {
      name: "Danh sách tờ khai",
      url: "/base/haiquan",
      icon: "icon-calculator"
    },
    // {
    //   name: "Tạo tờ khai thuế",
    //   url: "/base/declaration",
    //   icon: "icon-calculator"
    // },
    // {
    //   name: "Xác nhận HTNVT",
    //   url: "/base/xacnhanhtvnt",
    //   icon: "icon-calculator"
    // },
    {
      name: "Tra cứu tờ khai",
      url: "/base/tracuutk",
      icon: "icon-calculator"
    }
  ]
};

// Kho bạc
export const navigationTreasury = {
  items: [
    // {
    //   name: "Dashboard",
    //   url: "/dashboard",
    //   icon: "icon-speedometer",
    //   badge: {
    //     variant: "info",
    //     text: "NEW"
    //   }
    // },
    {
      name: "Danh sách nộp thuế",
      url: "/base/khobac",
      icon: "icon-calculator"
    },
    {
      name: "Tra cứu tờ khai",
      url: "/base/tracuutk",
      icon: "icon-calculator"
    }
  ]
};
