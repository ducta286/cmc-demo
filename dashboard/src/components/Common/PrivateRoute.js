import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { fakeAuth } from "./FakeAuth";

export const PrivateRoute = ({ component: Component, currentUser, currentUnit, socket, ...rest }) => (
    <Route {...rest} render={props => (
        localStorage.getItem('isAuthenticated')
            ? <Component 
                currentUser={localStorage.getItem('username')} 
                currentUnit={localStorage.getItem('unit')} 
                socket={socket} 
                {...props} />
            : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    )} />
)