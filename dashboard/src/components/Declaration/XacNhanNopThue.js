import React, { Component } from "react";
import "./Declaration.css";
import {
  FormGroup,
  ControlLabel,
  FormControl,
  Col,
  Button,
  Alert,
  Table
} from "react-bootstrap";
import Spinner from "../UI/Spinner/Spinner";
import DatePicker from "react-datepicker";
import moment from "moment";
import NumberFormat from "react-number-format";

import "react-datepicker/dist/react-datepicker.css";

import { query, invoke } from "../../api";
import axios from "axios";
import { SERVER_URL, SOCKET_URL } from "../../api_server";
import openSocket from "socket.io-client";

class XacNhanNopThue extends Component {
  state = {
    SO_CT: "",
    KYHIEU_CT: "",
    NGAY_CT: moment(),
    MA_KB: "",
    TEN_KB: "",
    MA_DV: "",
    TEN_DV: "",
    SO_TK: "",
    NAM_DK: "",
    SO_TIEN: 0,
    MA_NDKT: "",
    NDKT: "",
    TenDVXNK: [],
    KhoBac: [],
    message: "",
    loading: false,
    isShow: false
  };

  componentDidMount() {
    const SO_TK = new URLSearchParams(this.props.location.search).get("id");
    query("get_voucher_info", this.props.currentUser, "tax_declaration", SO_TK)
      .then(res => {
        console.log(res);
        this.setState({
          SO_TK: SO_TK,
          SO_CT_HQ: res.SO_CT,
          MA_HQ_PH: res.MA_HQ_PH,
          TEN_HQ_PH: res.TEN_HQ_PH,
          MA_HQ: res.MA_HQ,
          TEN_HQ: res.TEN_HQ,
          MA_DV: res.MA_DV,
          TEN_DV: res.TEN_DV,
          MA_LH: res.MA_LH,
          TEN_LH: res.TEN_LH,
          MA_KB: res.MA_KB,
          TEN_KB: res.TEN_KB,
          MA_HTVCHH: res.MA_HTVCHH,
          TEN_HTVCHH: res.TEN_HTVCHH,
          TKKB: res.TKKB,
          DUNO_TO: res.DUNO_TO,
          LOAI_THUE: res.LOAI_THUE,
          NGAY_DK: moment(res.NGAY_DK, "DD/MM/YYYY"),
          NGAY_HL: moment(res.NGAY_HL, "DD/MM/YYYY"),
          MA_CHUONG: res.MA_CHUONG,
          ST: JSON.parse(res.LOAI_THUE).filter(obj => obj.ST_MONEY !== 0)
        });
      })
      .catch(err => console.log(err));

    axios
      .get(`${SERVER_URL}/api/choicelist?choicelistType=company_list`)
      .then(res => {
        this.setState({
          TenDVXNK: res.data
        });
      })
      .catch(err => console.log(err));

    axios
      .get(`${SERVER_URL}/api/choicelist?choicelistType=treasury_list`)
      .then(res => {
        this.setState({
          KhoBac: res.data
        });
      })
      .catch(err => console.log(err));
  }

  handleChangeTENDVXNK = e => {
    this.setState({
      TEN_DV: e.target.selectedOptions[0].text,
      MA_DV: e.target.value
    });
  };

  handleChangeKhoBac = e => {
    this.setState({
      TEN_KB: e.target.selectedOptions[0].text,
      MA_KB: e.target.value
    });
  };

  handleDismissAlert = () => {
    this.setState({ isShow: false });
  };

  handleChangeNgayCT = date => {
    this.setState({
      NGAY_CT: date
    });
  };

  // Your custom transformation/validation
  myCustomFunction = rawValue => {
    const parsedDate = moment(rawValue, "DDMMYY");
    if (parsedDate.isValid()) {
      this.setState({ NGAY_CT: parsedDate });
    }
  };

  handleClickAccept = e => {
    e.preventDefault();
    this.setState({ loading: true });
    const data = {
      SO_CT: this.state.SO_CT,
      KYHIEU_CT: this.state.KYHIEU_CT,
      NGAY_CT: moment(this.state.NGAY_CT).format("DD/MM/YYYY"),
      MA_HQ: this.state.MA_HQ,
      MA_KB: this.state.MA_KB,
      TEN_KB: this.state.TEN_KB,
      MA_DV: this.state.MA_DV,
      TEN_DV: this.state.TEN_DV,
      SO_TK: this.state.SO_TK,
      NAM_DK: this.state.NAM_DK,
      MA_NDKT: this.state.MA_NDKT,
      NDKT: this.state.NDKT
    };

    console.log(data);

    const socket = openSocket(SOCKET_URL);

    invoke(
      "create_new_voucher",
      this.props.currentUser,
      "treasury_accounting",
      JSON.stringify(data)
    )
      .then(res => {
        // console.log(res);
        if (res.code == 2) { // Catch error from server
          this.setState({
            loading: false,
            message: res.details.split(":")[2].replace(")", ""),
            isShow: true
          });
        } else {
          if (res[0].status === "SUCCESS") {
            this.setState({
              loading: false,
              message: "SUCCESS",
              isShow: true
            });
            this.props.socket.emit("treasury-xacnhannopthue", { 
              message: "Hey, Haiquan have created XacNhanNopThue successfully!",
              socketid: this.props.socket.id,
              user: localStorage.getItem("username"),
              role: localStorage.getItem("role"),
              unitid: this.state.MA_HQ
             });
          } else {
            // console.log(res.details)
            this.setState({
              loading: false,
              message: "ERROR",
              isShow: true
            });
          }
        }
        
      })
      .catch(err => {
        console.log(err);
        this.setState({
          loading: false,
          message: "ERROR",
          isShow: true
        });
      });
  };

  handleDismissAlert = () => {
    this.setState({ isShow: false });
  };

  render() {
    let showAlert = null;
    let infoSummary = null;

    if (this.state.isShow) {
      if (this.state.message === "SUCCESS") {
        showAlert = (
          <Alert bsStyle="success">
            <strong>Chúc mừng!</strong> Bạn đã tạo mới dữ liệu thành công.
            <p className="dismiss-alert">
              <span onClick={this.handleDismissAlert}>x</span>
            </p>
          </Alert>
        );
      } else {
        showAlert = (
          <Alert bsStyle="danger">
            <strong>Lỗi!</strong> Xin vui lòng kiểm tra lại. {this.state.message}
            <p className="dismiss-alert">
              <span onClick={this.handleDismissAlert}>x</span>
            </p>
          </Alert>
        );
      }
    }

    if (!this.state.loading) {
      infoSummary = (
        <form>
          <h3>Xác Nhận Nộp Thuế</h3>
          <div className="row">
            <Col sm={2}>
              <FormGroup>
                <span>Số chứng từ</span>
                <FormControl
                  type="text"
                  value={this.state.SO_CT}
                  placeholder="Nhập"
                  onChange={e => this.setState({ SO_CT: e.target.value })}
                />
              </FormGroup>
            </Col>
            <Col sm={2}>
              <FormGroup>
                <span>Kí hiệu chứng từ</span>
                <FormControl
                  type="text"
                  value={this.state.KYHIEU_CT}
                  placeholder="Nhập"
                  onChange={e => this.setState({ KYHIEU_CT: e.target.value })}
                />
              </FormGroup>
            </Col>
            <Col sm={2}>
              <span>Ngày chứng từ</span>
              <DatePicker
                dateFormat="DD/MM/YYYY"
                selected={this.state.NGAY_CT}
                onChange={date => this.handleChangeNgayCT(date)}
                className="form-control"
                onChangeRaw={event => this.myCustomFunction(event.target.value)}
              />
            </Col>
          </div>

          <div className="row">
            <Col sm={8}>
              <FormGroup>
                <span>Kho bạc</span>
                <div className="row">
                  <Col sm={4}>
                    <FormControl
                      type="text"
                      value={this.state.MA_KB}
                      placeholder="Nhập"
                      onChange={e => this.setState({ MA_KB: e.target.value })}
                      onKeyDown={this.handleKeyPressMaKB}
                      disabled="true"
                    />
                  </Col>
                  <Col sm={8}>
                    <FormGroup controlId="formControlsSelect">
                      <FormControl
                        inputRef={el => (this.inputEl = el)}
                        componentClass="select"
                        placeholder="select"
                        value={this.state.MA_KB}
                        onChange={this.handleChangeKhoBac}
                        disabled="true"
                      >
                        <option value="">...</option>
                        {this.state.KhoBac.map(c => (
                          <option key={c.code} value={c.code}>
                            {c.name}
                          </option>
                        ))}
                      </FormControl>
                    </FormGroup>
                  </Col>
                </div>
              </FormGroup>
            </Col>
          </div>

          <div className="row">
            <Col sm={8}>
              <FormGroup>
                <span>Đơn vị xuất nhập khẩu</span>
                <div className="row">
                  <Col sm={4}>
                    <FormControl
                      type="text"
                      value={this.state.MA_DV}
                      placeholder="Nhập"
                      disabled="true"
                    />
                  </Col>
                  <Col sm={8}>
                    <FormGroup controlId="formControlsSelect">
                      <FormControl
                        inputRef={el => (this.inputEl = el)}
                        componentClass="select"
                        placeholder="select"
                        value={this.state.MA_DV}
                        onChange={this.handleChangeTENDVXNK}
                        disabled="true"
                      >
                        <option value="">...</option>
                        {this.state.TenDVXNK.map(c => (
                          <option key={c.code} value={c.code}>
                            {c.name}
                          </option>
                        ))}
                      </FormControl>
                    </FormGroup>
                  </Col>
                </div>
              </FormGroup>
            </Col>
          </div>

          <div className="row">
            <Col sm={4}>
              <FormGroup>
                <span>Số tờ khai</span>

                <FormControl
                  type="text"
                  value={this.state.SO_TK}
                  placeholder="Nhập"
                  onChange={e => this.setState({ SO_TK: e.target.value })}
                  disabled="true"
                />
              </FormGroup>
            </Col>
            <Col sm={4}>
              <FormGroup>
                <span>Năm đăng ký</span>

                <FormControl
                  type="text"
                  value={this.state.NAM_DK}
                  placeholder="Nhập"
                  onChange={e => this.setState({ NAM_DK: e.target.value })}
                />
              </FormGroup>
            </Col>
          </div>

          <div className="row">
            <Col sm={4}>
              <FormGroup>
                <span>Số tiền</span>
                <NumberFormat
                  value={this.state.SO_TIEN || 0}
                  thousandSeparator={true}
                  className="form-control money"
                  onValueChange={values => {
                    const { formattedValue, value, floatValue } = values;
                    this.setState({ SO_TIEN: floatValue || 0 });
                  }}
                />
              </FormGroup>
            </Col>
          </div>

          <div className="row">
            <Col sm={12}>
              <FormGroup>
                <span>Nội dung kinh tế</span>
                <div className="row">
                  <Col sm={2}>
                    <FormControl
                      type="text"
                      value={this.state.MA_NDKT}
                      onChange={e => this.setState({ MA_NDKT: e.target.value })}
                      placeholder="Nhập"
                    />
                  </Col>
                  <Col sm={10}>
                    <FormControl
                      type="text"
                      value={this.state.NDKT}
                      onChange={e => this.setState({ NDKT: e.target.value })}
                      placeholder="Nhập"
                    />
                  </Col>
                </div>
              </FormGroup>
            </Col>
          </div>

          {/* <hr /> */}

          <div className="ButtonGroup">
            <Button bsStyle="primary" onClick={this.handleClickAccept}>
              Cập nhật
            </Button>
            <Button bsStyle="primary">Thoát</Button>
          </div>
        </form>
      );
    } else {
      infoSummary = <Spinner />;
    }

    return (
      <div className="Declaration">
        {showAlert}
        {infoSummary}
      </div>
    );
  }
}

export default XacNhanNopThue;
