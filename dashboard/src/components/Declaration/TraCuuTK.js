import React, { Component } from "react";
import "./Declaration.css";
import {
  FormGroup,
  ControlLabel,
  FormControl,
  Col,
  Button,
  Alert,
  Table
} from "react-bootstrap";
import Spinner from "../UI/Spinner/Spinner";
import DatePicker from "react-datepicker";
import moment from "moment";
import NumberFormat from "react-number-format";

import "react-datepicker/dist/react-datepicker.css";

import { query, invoke } from "../../api";
import axios from "axios";
import { SERVER_URL } from "../../api_server";

class TraCuuTK extends Component {
  state = {
    SO_TK: "",
    NGAY_DK: moment(),
    MA_HQ: "",
    TEN_HQ: "",
    MA_LH: "",
    TEN_LH: "",
    MA_DV: "",
    TEN_DV: "",
    MA_KB: "",
    TEN_KB: "",
    TKKB: "",
    PS_NO: 0, // Phát sinh nợ
    PS_CO: 0, // Phát sinh có
    DUNO: 0,
    TRANS: [], // Thông tin về các giao dịch xảy ra,
    TenHQPH: [],
    TenHQ: [],
    SacThue: [],
    TenDVXNK: [],
    LHXNK: [],
    message: "",
    loading: false,
    isShow: false
  };

  componentDidMount() {
    axios
      .get(`${SERVER_URL}/api/choicelist?choicelistType=customs_unit`)
      .then(res => {
        this.setState({
          TenHQPH: res.data,
          TenHQ: res.data
        });
      })
      .catch(err => console.log(err));

    axios
      .get(`${SERVER_URL}/api/choicelist?choicelistType=tax_type`)
      .then(res => {
        this.setState({
          SacThue: res.data
        });
      })
      .catch(err => console.log(err));

    axios
      .get(`${SERVER_URL}/api/choicelist?choicelistType=company_list`)
      .then(res => {
        this.setState({
          TenDVXNK: res.data
        });
      })
      .catch(err => console.log(err));

    axios
      .get(`${SERVER_URL}/api/choicelist?choicelistType=type_code`)
      .then(res => {
        this.setState({
          LHXNK: res.data
        });
      })
      .catch(err => console.log(err));
  }

  getData = SO_TK => {
    let vourcher_type = "";
    const { currentUser, currentUnit } = this.props;
    const currentRole = localStorage.getItem("role");
    // console.log(currentRole);
    // switch (currentRole) {s
    //   case "customs":
    //     vourcher_type = "tax_declaration";
    //     break;
    //   case "customs":
    //     vourcher_type = "payment_request";
    //     break;
    //   case "customs":
    //     vourcher_type = "payment_confirm_reject";
    //     break;
    //   case "customs":
    //     vourcher_type = "tax_mission_confirm";
    //     break;
    // }

    query("get_voucher_info", currentUser, "tax_declaration", SO_TK)
      .then(res => {
        console.log(res);
        this.setState({
          NGAY_DK: moment(res.NGAY_DK, "DD/MM/YYYY"),
          MA_HQ: res.MA_HQ,
          TEN_HQ: res.TEN_HQ,
          MA_LH: res.MA_LH,
          TEN_LH: res.TEN_LH,
          MA_DV: res.MA_DV,
          TEN_DV: res.TEN_DV,
          MA_KB: res.MA_KB,
          TEN_KB: res.TEN_KB,
          TKKB: res.SO_TK,
          PS_NO: 0, // Phát sinh nợ
          PS_CO: 0, // Phát sinh có
          DUNO: res.SOTIEN
        });
      })
      .catch(err => console.log(err));

    query(
      "get_vouchers",
      this.props.currentUser,
      currentRole,
      this.props.currentUnit,
      SO_TK
    )
      .then(res => {
        console.log("TRANS", res);
        this.setState({ TRANS: res.data });
      })
      .catch(err => console.log(err));
  };

  handleKeyPressSoTK = e => {
    if (e.keyCode == 13 || e.keyCode == 9) {
      // let SO_TK = e.target.value;
      this.getData(this.state.SO_TK);
    }
  };

  handleChangeNgayDK = date => {
    this.setState({
      NGAY_DK: date
    });
  };

  myCustomFunction = rawValue => {
    const parsedDate = moment(rawValue, "DDMMYY");
    if (parsedDate.isValid()) {
      this.setState({ NGAY_DK: parsedDate });
    }
  };

  handleClickSearch = e => {
    e.preventDefault();
    this.getData(this.state.SO_TK);
  };

  handleClickBack = e => {
    this.props.history.goBack();
  };

  render() {
    let showAlert = null;
    let infoSummary = null;

    if (this.state.isShow) {
      if (this.state.message === "SUCCESS") {
        showAlert = (
          <Alert bsStyle="success">
            <strong>Chúc mừng!</strong> Bạn đã tạo mới dữ liệu thành công.
            <p className="dismiss-alert">
              <span onClick={this.handleDismissAlert}>x</span>
            </p>
          </Alert>
        );
      } else {
        showAlert = (
          <Alert bsStyle="danger">
            <strong>Lỗi!</strong> Xin vui lòng kiểm tra lại.
            <p className="dismiss-alert">
              <span onClick={this.handleDismissAlert}>x</span>
            </p>
          </Alert>
        );
      }
    }

    if (!this.state.loading) {
      infoSummary = (
        <form>
          <h3>Tra cứu tờ khai</h3>
          <div className="row">
            <Col sm={2}>
              <FormGroup>
                <ControlLabel>Số TK</ControlLabel>
                <FormControl
                  type="text"
                  value={this.state.SO_TK}
                  placeholder="Nhập"
                  onChange={e => this.setState({ SO_TK: e.target.value })}
                  onKeyDown={this.handleKeyPressSoTK}
                />
              </FormGroup>
            </Col>
            <Col sm={2}>
              <ControlLabel>Ngày ĐK</ControlLabel>
              <DatePicker
                dateFormat="DD/MM/YYYY"
                selected={this.state.NGAY_DK}
                onChange={date => this.handleChangeNgayDK(date)}
                className="form-control"
                onChangeRaw={event => this.myCustomFunction(event.target.value)}
                disabled
              />
            </Col>
          </div>

          <div className="row">
            <Col sm={8}>
              <FormGroup>
                <ControlLabel>Nơi mở tờ khai</ControlLabel>
                <div className="row">
                  <Col sm={4}>
                    <FormControl
                      type="text"
                      value={this.state.MA_HQ}
                      placeholder="Nhập"
                      onChange={e => this.setState({ MA_HQ: e.target.value })}
                      onKeyDown={this.handleKeyPressMaHQ}
                    />
                  </Col>
                  <Col sm={8}>
                    <FormGroup controlId="formControlsSelect">
                      <FormControl
                        inputRef={el => (this.inputEl = el)}
                        componentClass="select"
                        placeholder="select"
                        value={this.state.MA_HQ}
                        onChange={this.handleChangeTENHQ}
                      >
                        <option value="">...</option>
                        {this.state.TenHQ.map(c => (
                          <option key={c.code} value={c.code}>
                            {c.name}
                          </option>
                        ))}
                        >
                      </FormControl>
                    </FormGroup>
                  </Col>
                </div>
              </FormGroup>
            </Col>
          </div>

          <div className="row">
            <Col sm={8}>
              <FormGroup>
                <ControlLabel>Loại hình XNK</ControlLabel>
                <div className="row">
                  <Col sm={4}>
                    <FormControl
                      type="text"
                      value={this.state.MA_LH}
                      placeholder="Nhập"
                      onChange={e => this.setState({ MA_LH: e.target.value })}
                      onKeyDown={this.handleKeyPressMaLH}
                    />
                  </Col>
                  <Col sm={8}>
                    <FormGroup controlId="formControlsSelect">
                      <FormControl
                        inputRef={el => (this.inputEl = el)}
                        componentClass="select"
                        placeholder="select"
                        value={this.state.MA_LH}
                        onChange={this.handleChangeLHXNK}
                      >
                        <option value="">...</option>
                        {this.state.LHXNK.map(c => (
                          <option key={c.code} value={c.code}>
                            {c.name}
                          </option>
                        ))}
                      </FormControl>
                    </FormGroup>
                  </Col>
                </div>
              </FormGroup>
            </Col>
          </div>

          <div className="row">
            <Col sm={8}>
              <FormGroup>
                <ControlLabel>Đơn vị xuất nhập khẩu</ControlLabel>
                <div className="row">
                  <Col sm={4}>
                    <FormControl
                      type="text"
                      value={this.state.MA_DV}
                      placeholder="Nhập"
                      onChange={e => this.setState({ MA_DV: e.target.value })}
                      onKeyDown={this.handleKeyPressMaDV}
                    />
                  </Col>
                  <Col sm={8}>
                    <FormGroup controlId="formControlsSelect">
                      <FormControl
                        inputRef={el => (this.inputEl = el)}
                        componentClass="select"
                        placeholder="select"
                        value={this.state.MA_DV}
                        onChange={this.handleChangeTENDVXNK}
                      >
                        <option value="">...</option>
                        {this.state.TenDVXNK.map(c => (
                          <option key={c.code} value={c.code}>
                            {c.name}
                          </option>
                        ))}
                      </FormControl>
                    </FormGroup>
                  </Col>
                </div>
              </FormGroup>
            </Col>
          </div>

          <div className="row">
            <Col sm={8}>
              <ControlLabel>Kho bạc</ControlLabel>
              <div className="row">
                <Col sm={4}>
                  <FormControl
                    type="text"
                    value={this.state.MA_KB}
                    placeholder="Nhập"
                    onChange={e => this.setState({ MA_KB: e.target.value })}
                  />
                </Col>
                <Col sm={8}>
                  <FormControl
                    type="text"
                    value={this.state.TEN_KB}
                    placeholder="Nhập"
                    onChange={e => this.setState({ TEN_KB: e.target.value })}
                  />
                </Col>
              </div>
            </Col>
            <Col sm={2}>
              <ControlLabel>Tài khoản KB</ControlLabel>
              <FormGroup controlId="formControlsSelect">
                <FormControl
                  inputRef={el => (this.inputEl = el)}
                  componentClass="select"
                  placeholder="select"
                >
                  <option value="KB01000001111">01000001111</option>
                  <option value="KB01000001222">01000001222</option>
                  <option value="KB01000001333">01000001333</option>
                  <option value="KB01000001444">01000001444</option>
                </FormControl>
              </FormGroup>
            </Col>
          </div>

          <div className="row">
            <Col sm={4}>
              <ControlLabel>Phát sinh nợ</ControlLabel>
              <FormControl
                type="text"
                value={this.state.PS_NO}
                placeholder="Nhập"
                onChange={e => this.setState({ PS_NO: e.target.value })}
              />
            </Col>
            <Col sm={4}>
              <ControlLabel>Phát sinh có</ControlLabel>
              <FormControl
                type="text"
                value={this.state.PS_CO}
                placeholder="Nhập"
                onChange={e => this.setState({ PS_CO: e.target.value })}
              />
            </Col>
            <Col sm={4}>
              <ControlLabel>Dư nợ</ControlLabel>
              <FormControl
                type="text"
                value={this.state.DUNO}
                placeholder="Nhập"
                onChange={e => this.setState({ DUNO: e.target.value })}
              />
            </Col>
          </div>

          <div className="row">
            <Table responsive>
              <thead>
                <tr>
                  <th />
                  <th />
                </tr>
              </thead>
              <tbody>
                {this.state.TRANS.map((tran, index) => {
                  let dateModified = moment(parseInt(tran.CREATED_DATE)).format(
                    "DD/MM/YYYY hh:mm:ss"
                  );

                  let status = null;
                  switch (tran.VOUCHER_TYPE) {
                    case "tax_declaration":
                      status = "TBT/thông tin thuế của tờ khai";
                      break;
                    case "payment_request":
                      status = "Yêu cầu thanh toán";
                      break;
                    case "payment_confirm_reject":
                      status = "Xác nhận thanh toán";
                      break;
                    case "tax_mission_confirm":
                      status = "Xác nhận hoàn thành nghĩa vụ thuế";
                      break;
                    case "treasury_accounting":
                      status = "Hoạch toán kho bạc";
                      break;
                    default:
                      break;
                  }

                  return (
                    <tr key={index}>
                      <td>{dateModified}</td>
                      <td>{status}</td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          </div>

          {/* <hr /> */}

          <div className="ButtonGroup">
            <Button bsStyle="primary" onClick={this.handleClickSearch}>
              Tra cứu
            </Button>
            <Button bsStyle="primary" onClick={this.handleClickBack}>
              Thoát
            </Button>
          </div>
        </form>
      );
    } else {
      infoSummary = <Spinner />;
    }

    return (
      <div className="Declaration">
        {showAlert}
        {infoSummary}
      </div>
    );
  }
}

export default TraCuuTK;
