import React, { Component } from "react";
import "./Declaration.css";
import {
  FormGroup,
  ControlLabel,
  FormControl,
  Col,
  Button,
  Alert
} from "react-bootstrap";
import Spinner from "../UI/Spinner/Spinner";
import DatePicker from "react-datepicker";
import moment from "moment";
import NumberFormat from "react-number-format";

import "react-datepicker/dist/react-datepicker.css";

import { query, invoke } from "../../api";
import axios from "axios";
import { SERVER_URL, SOCKET_URL } from "../../api_server";
import openSocket from "socket.io-client";

class Declaration extends Component {
  state = {
    MA_HQ_PH: "",
    TEN_HQ_PH: "",
    SO_CT: 0,
    NGAY_HL: moment(),
    MA_HQ: "",
    TEN_HQ: "",
    SO_TK: 0,
    NGAY_DK: moment(),
    MA_DV: "",
    TEN_DV: "",
    MA_CHUONG: "",
    MA_LH: "",
    TEN_LH: "",
    MA_HTVCHH: 0,
    TEN_HTVCHH: "",
    MA_KB: "",
    TEN_KB: "",
    TKKB: "",
    LOAI_THUE: "",
    DUNO: 0,
    DUNO_TO: 0,
    // MONEY_ST1: 0,
    // MONEY_ST2: 0,
    // MONEY_ST3: 0,
    // MONEY_ST4: 0,
    // MONEY_ST5: 0,
    value: "",
    ST: [
      {
        ST_KEY: "",
        ST_MONEY: 0
      },
      {
        ST_KEY: "",
        ST_MONEY: 0
      },
      {
        ST_KEY: "",
        ST_MONEY: 0
      },
      {
        ST_KEY: "",
        ST_MONEY: 0
      },
      {
        ST_KEY: "",
        ST_MONEY: 0
      }
    ],
    TenHQPH: [],
    TenHQ: [],
    SacThue: [],
    TenDVXNK: [],
    LHXNK: [],
    KhoBac: [],
    message: "",
    loading: false,
    isShow: false
  };

  componentDidMount() {
    const { currentUnit } = this.props;
    // console.log(currentUnit);
    this.setState({ MA_HQ: currentUnit });

    axios
      .get(`${SERVER_URL}/api/choicelist?choicelistType=customs_unit`)
      .then(res => {
        this.setState({
          TenHQPH: res.data,
          TenHQ: res.data
        });
      })
      .catch(err => console.log(err));

    axios
      .get(`${SERVER_URL}/api/choicelist?choicelistType=tax_type`)
      .then(res => {
        this.setState({
          SacThue: res.data
        });
      })
      .catch(err => console.log(err));

    axios
      .get(`${SERVER_URL}/api/choicelist?choicelistType=company_list`)
      .then(res => {
        this.setState({
          TenDVXNK: res.data
        });
      })
      .catch(err => console.log(err));

    axios
      .get(`${SERVER_URL}/api/choicelist?choicelistType=type_code`)
      .then(res => {
        this.setState({
          LHXNK: res.data
        });
      })
      .catch(err => console.log(err));

    axios
      .get(`${SERVER_URL}/api/choicelist?choicelistType=treasury_list`)
      .then(res => {
        this.setState({
          KhoBac: res.data
        });
      })
      .catch(err => console.log(err));
  }

  /*
   * Xử lý các sự kiện Select Dropdown ...
  */
  handleChangeTENHQPH = e => {
    this.setState({
      TEN_HQ_PH: e.target.selectedOptions[0].text,
      MA_HQ_PH: e.target.value
    });
  };

  handleChangeTENHQ = e => {
    this.setState({
      TEN_HQ: e.target.selectedOptions[0].text,
      MA_HQ: e.target.value
    });
  };

  handleChangeTENDVXNK = e => {
    this.setState({
      TEN_DV: e.target.selectedOptions[0].text,
      MA_DV: e.target.value
    });
  };

  handleChangeLHXNK = e => {
    this.setState({
      TEN_LH: e.target.selectedOptions[0].text,
      MA_LH: e.target.value
    });
  };

  handleChangeKhoBac = e => {
    this.setState({
      TEN_KB: e.target.selectedOptions[0].text,
      MA_KB: e.target.value
    });
  };

  handleChangeNgayHL = date => {
    // console.log(moment(date).format("DD/MM/YYYY"));
    this.setState({
      NGAY_HL: date
    });
  };

  handleChangeNgayDK = date => {
    this.setState({
      NGAY_DK: date
    });
  };

  // Your custom transformation/validation
  myCustomFunction = rawValue => {
    const parsedDate = moment(rawValue, "DDMMYY");
    if (parsedDate.isValid()) {
      this.setState({ NGAY_HL: parsedDate });
    }
  };

  handleKeyPressMaHQPH = e => {
    if (e.keyCode == 13 || e.keyCode == 9) {
      this.setState({ TEN_HQ_PH: e.target.value });
    }
  };

  handleKeyPressMaHQ = e => {
    if (e.keyCode == 13 || e.keyCode == 9) {
      this.setState({ TEN_HQ: e.target.value });
    }
  };

  handleKeyPressMaDV = e => {
    if (e.keyCode == 13 || e.keyCode == 9) {
      this.setState({ TEN_DV: e.target.value });
    }
  };

  handleKeyPressMaLH = e => {
    if (e.keyCode == 13 || e.keyCode == 9) {
      this.setState({ TEN_LH: e.target.value });
    }
  };

  // handleBlurMaHQPH = e => {
  //   console.log(e.target.value);
  //   // this.setState({ TenHQPH: this.bookOptions });
  // };

  // handleBlurMaHQ = e => {
  //   console.log(e.target.value);
  //   // this.setState({ TenHQ: this.bookOptions });
  // };

  handleSubmit = e => {
    e.preventDefault();
    this.setState({ loading: true });
    const data = {
      MA_HQ_PH: this.state.MA_HQ_PH,
      TEN_HQ_PH: this.state.TEN_HQ_PH,
      SO_CT: this.state.SO_CT,
      NGAY_HL: moment(this.state.NGAY_HL).format("DD/MM/YYYY"),
      MA_HQ: this.state.MA_HQ,
      TEN_HQ: this.state.TEN_HQ,
      SO_TK: this.state.SO_TK,
      NGAY_DK: moment(this.state.NGAY_DK).format("DD/MM/YYYY"),
      MA_DV: this.state.MA_DV,
      TEN_DV: this.state.TEN_DV,
      MA_CHUONG: this.state.MA_CHUONG,
      MA_LH: this.state.MA_LH,
      TEN_LH: this.state.TEN_LH,
      MA_HTVCHH: this.state.MA_HTVCHH,
      TEN_HTVCHH: this.state.TEN_HTVCHH,
      MA_KB: this.state.MA_KB,
      TEN_KB: this.state.TEN_KB,
      TKKB: this.state.TKKB,
      LOAI_THUE: JSON.stringify(this.state.ST),
      DUNO: 0,
      DUNO_TO:
        parseInt(this.state.ST[0].ST_MONEY, 10) +
        parseInt(this.state.ST[1].ST_MONEY, 10) +
        parseInt(this.state.ST[2].ST_MONEY, 10) +
        parseInt(this.state.ST[3].ST_MONEY, 10) +
        parseInt(this.state.ST[4].ST_MONEY, 10)
    };

    console.log(data);

    // // Connect Socket
    // const socket = openSocket(SOCKET_URL);

    invoke(
      "create_new_voucher",
      this.props.currentUser,
      "tax_declaration",
      JSON.stringify(data)
    )
      .then(res => {
        // console.log("Not catch", res);
        if (res.code == 2) { // Catch error from server
          this.setState({
            loading: false,
            message: res.details.split(":")[2].replace(")", ""),
            isShow: true
          });
        } else {
          if (res[0].status === "SUCCESS") {
            this.setState({
              loading: false,
              message: "SUCCESS",
              isShow: true
            });
            // Send message to Server
            this.props.socket.emit("customs-declaration", { 
              socketid: this.props.socket.id,
              message: "Hey, Haiquan have created TKThue successfully!",
              user: localStorage.getItem("username"),
              role: localStorage.getItem("role"),
              unitid: this.state.MA_DV
             });
          } else {
            // console.log(res);
            this.setState({
              loading: false,
              message: "ERROR",
              isShow: true
            });
          }
        }
        

        // this.props.history.push(`/company`);
      })
      .catch((err) => {
        console.log("Catch error:", err);
        this.setState({
          loading: false,
          message: "ERROR",
          isShow: true
        });
      });
  };

  handleClickCancel = () => {
    this.setState({
      MA_HQ_PH: "",
      TEN_HQ_PH: "",
      SO_CT: 0,
      NGAY_HL: moment(),
      MA_HQ: "",
      TEN_HQ: "",
      SO_TK: 0,
      NGAY_DK: moment(),
      MA_DV: "",
      TEN_DV: "",
      MA_CHUONG: "",
      MA_LH: "",
      TEN_LH: "",
      MA_HTVCHH: 0,
      TEN_HTVCHH: "",
      MA_KB: "",
      TEN_KB: "",
      TKKB: "",
      LOAI_THUE: "",
      DUNO: 0,
      DUNO_TO: 0
    });
  };

  handleDismissAlert = () => {
    this.setState({ isShow: false });
  };

  handleClickBack = () => {
    this.props.history.goBack();
  };

  render() {
    let showAlert = null;
    let infoSummary = null;

    if (this.state.isShow) {
      if (this.state.message === "SUCCESS") {
        showAlert = (
          <Alert bsStyle="success">
            <strong>Chúc mừng!</strong> Bạn đã tạo mới dữ liệu thành công.
            <p className="dismiss-alert">
              <span onClick={this.handleDismissAlert}>x</span>
            </p>
          </Alert>
        );
      } else {
        showAlert = (
          <Alert bsStyle="danger">
            <strong>Lỗi!</strong> Xin vui lòng kiểm tra lại. {this.state.message}
            <p className="dismiss-alert">
              <span onClick={this.handleDismissAlert}>x</span>
            </p>
          </Alert>
        );
      }
    }

    if (!this.state.loading) {
      infoSummary = (
        <form>
          <h3>Nhập thông tin thuế của tờ khai</h3>
          <div className="row">
            <Col sm={8}>
              {/* <FormGroup> */}
              Nơi phát hành chứng từ
              <div className="row">
                <Col sm={4}>
                  <FormControl
                    type="text"
                    value={this.state.MA_HQ_PH}
                    placeholder="Nhập"
                    onChange={e => this.setState({ MA_HQ_PH: e.target.value })}
                    onKeyDown={this.handleKeyPressMaHQPH}
                  />
                </Col>
                <Col sm={8}>
                  <FormGroup controlId="formControlsSelect">
                    <FormControl
                      inputRef={el => (this.inputEl = el)}
                      componentClass="select"
                      placeholder="select"
                      value={this.state.MA_HQ_PH}
                      onChange={this.handleChangeTENHQPH}
                    >
                      <option value="">...</option>
                      {this.state.TenHQPH.map(c => (
                        <option key={c.code} value={c.code}>
                          {c.name}
                        </option>
                      ))}
                    </FormControl>
                  </FormGroup>
                </Col>
              </div>
              {/* </FormGroup> */}
            </Col>
            <Col sm={2}>
              <FormGroup>
                Số chứng từ
                <FormControl
                  type="text"
                  value={this.state.SO_CT}
                  placeholder="Nhập"
                  onChange={e => this.setState({ SO_CT: e.target.value })}
                />
              </FormGroup>
            </Col>
            <Col sm={2}>
              Ngày HL
              {/* <FormControl
              type="text"
              value="01/07/2018"
              placeholder="Nhập"
            /> */}
              <DatePicker
                dateFormat="DD/MM/YYYY"
                selected={this.state.NGAY_HL}
                onChange={date => this.handleChangeNgayHL(date)}
                className="form-control"
                onChangeRaw={event => this.myCustomFunction(event.target.value)}
              />
            </Col>
          </div>

          <div className="row">
            <Col sm={8}>
              <FormGroup>
                Nơi mở tờ khai
                <div className="row">
                  <Col sm={4}>
                    <FormControl
                      type="text"
                      value={this.state.MA_HQ}
                      placeholder="Nhập"
                      onChange={e => this.setState({ MA_HQ: e.target.value })}
                      onKeyDown={this.handleKeyPressMaHQ}
                      disabled="true"
                    />
                  </Col>
                  <Col sm={8}>
                    <FormGroup controlId="formControlsSelect">
                      <FormControl
                        inputRef={el => (this.inputEl = el)}
                        componentClass="select"
                        placeholder="select"
                        value={this.state.MA_HQ}
                        onChange={this.handleChangeTENHQ}
                        disabled="true"
                      >
                        <option value="">...</option>
                        {this.state.TenHQ.map(c => (
                          <option key={c.code} value={c.code}>
                            {c.name}
                          </option>
                        ))}
                        >
                      </FormControl>
                    </FormGroup>
                  </Col>
                </div>
              </FormGroup>
            </Col>
          </div>

          <div className="row">
            <Col sm={8}>
              <FormGroup>
                Loại hình XNK
                <div className="row">
                  <Col sm={4}>
                    <FormControl
                      type="text"
                      value={this.state.MA_LH}
                      placeholder="Nhập"
                      onChange={e => this.setState({ MA_LH: e.target.value })}
                      onKeyDown={this.handleKeyPressMaLH}
                    />
                  </Col>
                  <Col sm={8}>
                    {/* <FormControl
                    type="text"
                    disabled="true"
                    value={this.state.TEN_LH}
                    placeholder="..."
                  /> */}
                    <FormGroup controlId="formControlsSelect">
                      <FormControl
                        inputRef={el => (this.inputEl = el)}
                        componentClass="select"
                        placeholder="select"
                        value={this.state.MA_LH}
                        onChange={this.handleChangeLHXNK}
                      >
                        <option value="">...</option>
                        {this.state.LHXNK.map(c => (
                          <option key={c.code} value={c.code}>
                            {c.name}
                          </option>
                        ))}
                      </FormControl>
                    </FormGroup>
                  </Col>
                </div>
              </FormGroup>
            </Col>
          </div>

          <div className="row">
            <Col sm={8}>
              <FormGroup>
                Đơn vị xuất nhập khẩu
                <div className="row">
                  <Col sm={4}>
                    <FormControl
                      type="text"
                      value={this.state.MA_DV}
                      placeholder="Nhập"
                      onChange={e => this.setState({ MA_DV: e.target.value })}
                      onKeyDown={this.handleKeyPressMaDV}
                    />
                  </Col>
                  <Col sm={8}>
                    {/* <FormControl
                    type="text"
                    disabled="true"
                    value={this.state.TEN_DV}
                    placeholder="..."
                    onChange={e => this.setState({ TEN_DV: e.target.value })} */}
                    <FormGroup controlId="formControlsSelect">
                      <FormControl
                        inputRef={el => (this.inputEl = el)}
                        componentClass="select"
                        placeholder="select"
                        value={this.state.MA_DV}
                        onChange={this.handleChangeTENDVXNK}
                      >
                        <option value="">...</option>
                        {this.state.TenDVXNK.map(c => (
                          <option key={c.code} value={c.code}>
                            {c.name}
                          </option>
                        ))}
                      </FormControl>
                    </FormGroup>
                  </Col>
                </div>
              </FormGroup>
            </Col>
            <Col sm={2}>
              <FormGroup>
                Mã chương
                <FormControl
                  type="text"
                  value={this.state.MA_CHUONG}
                  placeholder="Nhập"
                  onChange={e => this.setState({ MA_CHUONG: e.target.value })}
                />
              </FormGroup>
            </Col>
          </div>

          <div className="row">
            <Col sm={2}>
              <FormGroup>
                Số TK
                <FormControl
                  type="text"
                  value={this.state.SO_TK}
                  placeholder="Nhập"
                  onChange={e => this.setState({ SO_TK: e.target.value })}
                  required
                />
              </FormGroup>
            </Col>
            <Col sm={2}>
              <FormGroup>
                Ngày ĐK
                {/* <FormControl
                type="text"
                value="01/07/2018"
                placeholder="Nhập"
              /> */}
                <DatePicker
                  dateFormat="DD/MM/YYYY"
                  selected={this.state.NGAY_DK}
                  onChange={this.handleChangeNgayDK}
                  className="form-control"
                  onChangeRaw={event =>
                    this.myCustomFunction(event.target.value)}
                />
              </FormGroup>
            </Col>
            <Col sm={8}>
              <FormGroup>
                Hình thức vận chuyển hàng hóa
                <div className="row">
                  <Col sm={4}>
                    <FormControl
                      type="text"
                      disabled="true"
                      value="2"
                      placeholder="Nhập"
                    />
                  </Col>
                  <Col sm={8}>
                    <FormGroup controlId="formControlsSelect">
                      <FormControl
                        inputRef={el => (this.inputEl = el)}
                        componentClass="select"
                        placeholder="select"
                      >
                        <option value="">Không qua biên giới đất liền</option>
                        <option value="unknown">Unknown</option>
                      </FormControl>
                    </FormGroup>
                  </Col>
                </div>
              </FormGroup>
            </Col>
          </div>

          <div className="row">
            <Col sm={8}>
              <FormGroup>
                Kho bạc
                <div className="row">
                  <Col sm={4}>
                    <FormControl
                      type="text"
                      value={this.state.MA_KB}
                      placeholder="Nhập"
                      onChange={e => this.setState({ MA_KB: e.target.value })}
                      onKeyDown={this.handleKeyPressMaKB}
                    />
                  </Col>
                  <Col sm={8}>
                    <FormGroup controlId="formControlsSelect">
                      <FormControl
                        inputRef={el => (this.inputEl = el)}
                        componentClass="select"
                        placeholder="select"
                        value={this.state.MA_KB}
                        onChange={this.handleChangeKhoBac}
                      >
                        <option value="">...</option>
                        {this.state.KhoBac.map(c => (
                          <option key={c.code} value={c.code}>
                            {c.name}
                          </option>
                        ))}
                      </FormControl>
                    </FormGroup>
                  </Col>
                </div>
              </FormGroup>
            </Col>
            <Col sm={2}>
              Tài khoản KB
              <FormGroup controlId="formControlsSelect">
                <FormControl
                  inputRef={el => (this.inputEl = el)}
                  componentClass="select"
                  placeholder="select"
                  value={this.state.TKKB}
                  onChange={e => this.setState({ TKKB: e.target.value })}
                >
                  <option value="">...</option>
                  <option value="KB01000001111">01000001111</option>
                  <option value="KB01000001222">01000001222</option>
                  <option value="KB01000001333">01000001333</option>
                  <option value="KB01000001444">01000001444</option>
                </FormControl>
              </FormGroup>
            </Col>
          </div>

          <div className="row">
            <Col sm={3}>
              Sắc thuế
              <FormGroup controlId="formControlsSelect">
                <FormControl
                  inputRef={el => (this.inputEl = el)}
                  componentClass="select"
                  placeholder="select"
                  value={this.state.ST[0].ST_KEY}
                  onChange={e => {
                    const ST = Object.assign(this.state.ST); // Pull the entire items object out. Using object.assign is a good idea for objects.
                    ST[0].ST_KEY = e.target.value; // update the items object as needed
                    this.setState({ ST }); // Put back in state
                  }}
                >
                  <option value="">...</option>
                  {this.state.SacThue.map(c => (
                    <option key={c.code} value={c.prefix}>
                      {c.name}
                    </option>
                  ))}
                </FormControl>
              </FormGroup>
            </Col>
            <Col sm={2}>
              Số tiền
              {/* <FormControl
              type="text"
              value={this.state.MONEY_ST1 || 0}
              className="money"
              onChange={e =>
                this.setState({ MONEY_ST1: e.target.value || 0 })}
            /> */}
              <NumberFormat
                value={this.state.ST[0].ST_MONEY || 0}
                thousandSeparator={true}
                className="form-control money"
                onValueChange={values => {
                  const { formattedValue, value, floatValue } = values;
                  const ST = Object.assign(this.state.ST); // Pull the entire items object out. Using object.assign is a good idea for objects.
                  ST[0].ST_MONEY = floatValue; // update the items object as needed
                  this.setState({ ST }); // Put back in state
                  {
                    /* this.setState({MONEY_ST1: floatValue || 0}); */
                  }
                }}
              />
            </Col>
          </div>

          <div className="row">
            <Col sm={3}>
              <FormGroup controlId="formControlsSelect">
                <FormControl
                  inputRef={el => (this.inputEl = el)}
                  componentClass="select"
                  placeholder="select"
                  value={this.state.ST[1].ST_KEY}
                  onChange={e => {
                    const ST = Object.assign(this.state.ST);
                    ST[1].ST_KEY = e.target.value;
                    this.setState({ ST });
                  }}
                >
                  <option value="">...</option>
                  {this.state.SacThue.map(c => (
                    <option key={c.code} value={c.prefix}>
                      {c.name}
                    </option>
                  ))}
                </FormControl>
              </FormGroup>
            </Col>
            <Col sm={2}>
              <NumberFormat
                value={this.state.ST[1].ST_MONEY || 0}
                thousandSeparator={true}
                className="form-control money"
                onValueChange={values => {
                  const { formattedValue, value, floatValue } = values;
                  const ST = Object.assign(this.state.ST);
                  ST[1].ST_MONEY = floatValue;
                  this.setState({ ST });
                }}
              />
            </Col>
          </div>

          <div className="row">
            <Col sm={3}>
              <FormGroup controlId="formControlsSelect">
                <FormControl
                  inputRef={el => (this.inputEl = el)}
                  componentClass="select"
                  placeholder="select"
                  value={this.state.ST[2].ST_KEY}
                  onChange={e => {
                    const ST = Object.assign(this.state.ST);
                    ST[2].ST_KEY = e.target.value;
                    this.setState({ ST });
                  }}
                >
                  <option value="">...</option>
                  {this.state.SacThue.map(c => (
                    <option key={c.code} value={c.prefix}>
                      {c.name}
                    </option>
                  ))}
                </FormControl>
              </FormGroup>
            </Col>
            <Col sm={2}>
              <NumberFormat
                value={this.state.ST[2].ST_MONEY || 0}
                thousandSeparator={true}
                className="form-control money"
                onValueChange={values => {
                  const { formattedValue, value, floatValue } = values;
                  const ST = Object.assign(this.state.ST);
                  ST[2].ST_MONEY = floatValue;
                  this.setState({ ST });
                }}
              />
            </Col>
          </div>

          <div className="row">
            <Col sm={3}>
              <FormGroup controlId="formControlsSelect">
                <FormControl
                  inputRef={el => (this.inputEl = el)}
                  componentClass="select"
                  placeholder="select"
                  value={this.state.ST[3].ST_KEY}
                  onChange={e => {
                    const ST = Object.assign(this.state.ST);
                    ST[3].ST_KEY = e.target.value;
                    this.setState({ ST });
                  }}
                >
                  <option value="">...</option>
                  {this.state.SacThue.map(c => (
                    <option key={c.code} value={c.prefix}>
                      {c.name}
                    </option>
                  ))}
                </FormControl>
              </FormGroup>
            </Col>
            <Col sm={2}>
              <NumberFormat
                value={this.state.ST[3].ST_MONEY || 0}
                thousandSeparator={true}
                className="form-control money"
                onValueChange={values => {
                  const { formattedValue, value, floatValue } = values;
                  const ST = Object.assign(this.state.ST);
                  ST[3].ST_MONEY = floatValue;
                  this.setState({ ST });
                }}
              />
            </Col>
          </div>

          <div className="row">
            <Col sm={3}>
              <FormGroup controlId="formControlsSelect">
                <FormControl
                  inputRef={el => (this.inputEl = el)}
                  componentClass="select"
                  placeholder="select"
                  value={this.state.ST[4].ST_KEY}
                  onChange={e => {
                    const ST = Object.assign(this.state.ST);
                    ST[4].ST_KEY = e.target.value;
                    this.setState({ ST });
                  }}
                >
                  <option value="">...</option>
                  {this.state.SacThue.map(c => (
                    <option key={c.code} value={c.prefix}>
                      {c.name}
                    </option>
                  ))}
                </FormControl>
              </FormGroup>
            </Col>
            <Col sm={2}>
              <NumberFormat
                value={this.state.ST[4].ST_MONEY || 0}
                thousandSeparator={true}
                className="form-control money"
                onValueChange={values => {
                  const { formattedValue, value, floatValue } = values;
                  const ST = Object.assign(this.state.ST);
                  ST[4].ST_MONEY = floatValue;
                  this.setState({ ST });
                }}
              />
            </Col>
          </div>

          <div className="row">
            <Col sm={3}>
              <ControlLabel>Tổng cộng</ControlLabel>
            </Col>
            <Col sm={2}>
              {/* <FormControl
              type="text"
              disabled="true"
              value={
                parseInt(this.state.MONEY_ST1, 10) +
                parseInt(this.state.MONEY_ST2, 10) +
                parseInt(this.state.MONEY_ST3, 10) +
                parseInt(this.state.MONEY_ST4, 10) +
                parseInt(this.state.MONEY_ST5, 10)
              }
              className="money"
            /> */}
              <NumberFormat
                disabled="true"
                value={
                  parseInt(this.state.ST[0].ST_MONEY, 10) +
                  parseInt(this.state.ST[1].ST_MONEY, 10) +
                  parseInt(this.state.ST[2].ST_MONEY, 10) +
                  parseInt(this.state.ST[3].ST_MONEY, 10) +
                  parseInt(this.state.ST[4].ST_MONEY, 10)
                }
                thousandSeparator={true}
                className="form-control money"
                type="text"
              />
            </Col>
          </div>

          {/* <hr /> */}

          <div className="ButtonGroup">
            <Button bsStyle="primary" onClick={this.handleSubmit}>
              Cập nhật
            </Button>
            <Button bsStyle="primary" onClick={this.handleClickCancel}>
              Hủy bỏ
            </Button>
            <Button bsStyle="primary" onClick={this.handleClickBack}>
              Thoát
            </Button>
          </div>
        </form>
      );
    } else {
      infoSummary = <Spinner />;
    }

    return (
      <div className="Declaration">
        {showAlert}
        {infoSummary}
      </div>
    );
  }
}

export default Declaration;
