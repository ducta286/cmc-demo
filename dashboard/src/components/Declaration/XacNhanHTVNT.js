import React, { Component } from "react";
import "./Declaration.css";
import {
  FormGroup,
  ControlLabel,
  FormControl,
  Col,
  Button,
  Alert,
  Table
} from "react-bootstrap";
import Spinner from "../UI/Spinner/Spinner";
import DatePicker from "react-datepicker";
import moment from "moment";
import NumberFormat from "react-number-format";

import "react-datepicker/dist/react-datepicker.css";

import { query, invoke } from "../../api";
import axios from "axios";
import { SERVER_URL, SOCKET_URL } from "../../api_server";
import openSocket from "socket.io-client";

class XacNhanHTVNT extends Component {
  state = {
    MA_HQ_PH: "",
    TEN_HQ_PH: "",
    SO_CT: 0,
    NGAY_HL: moment(),
    MA_HQ: "",
    TEN_HQ: "",
    SO_TK: 0,
    NGAY_DK: moment(),
    MA_DV: "",
    TEN_DV: "",
    MA_CHUONG: "",
    MA_LH: "",
    TEN_LH: "",
    MA_HTVCHH: 0,
    TEN_HTVCHH: "",
    MA_KB: "",
    TEN_KB: "",
    TKKB: "",
    LOAI_THUE: "",
    DUNO: 0,
    DUNO_TO: 0,
    MONEY_ST1: 0,
    MONEY_ST2: 0,
    MONEY_ST3: 0,
    MONEY_ST4: 0,
    MONEY_ST5: 0,
    value: "",
    TenHQPH: [],
    TenHQ: [],
    SacThue: [],
    TenDVXNK: [],
    LHXNK: [],
    TenNH: [],
    KhoBac: [],
    SO_CT_NHAP: "",
    KHCT: "",
    KHCT_NHAP: "",
    NGAY_LAP: moment(),
    MA_NH_PH: "",
    TEN_NH_PH: "",
    MA_NH_TH: "",
    TEN_NH_TH: "",
    message: "",
    ST: [],
    loading: false,
    isShow: false
  };

  componentDidMount() {
    const SO_TK = new URLSearchParams(this.props.location.search).get("id");
    query(
      "get_voucher_info",
      this.props.currentUser,
      "payment_confirm_reject",
      SO_TK
    )
      .then(res => {
        console.log("RESPONSE", res);
        this.setState({
          KHCT: res.KYHIEU_CT,
          MA_CHUONG: res.MA_CHUONG,
          MA_DV: res.MA_DV,
          MA_HQ: res.MA_HQ,
          MA_KB: res.MA_KB,
          MA_LH: res.MA_LH,
          MA_NH_PH: res.MA_NH_PH,
          MA_NH_TH: res.MA_NH_TH,
          // MA_ST: res.MA_ST,
          NGAY_CT: moment(res.NGAY_CT, "DD/MM/YYYY"),
          NGAY_HL: moment(res.NGAY_HL, "DD/MM/YYYY"),
          DUNO_TO: res.SOTIEN,
          SO_CT: res.SO_CT,
          SO_TK: res.SO_TK,
          TEN_DV: res.TEN_DV,
          TEN_HQ: res.TEN_HQ,
          TEN_KB: res.TEN_KB,
          TEN_LH: res.TEN_LH,
          TEN_NH_PH: res.TEN_NH_PH_TH,
          TKKB: res.TKKB,
          ST: JSON.parse(res.MA_ST).filter(obj => obj.ST_MONEY !== 0)
        });
      })
      .catch(err => console.log(err));

    axios
      .get(`${SERVER_URL}/api/choicelist?choicelistType=bank_list`)
      .then(res => {
        console.log("TenNH", res);
        this.setState({
          TenNH: res.data
        });
      })
      .catch(err => console.log(err));

    axios
      .get(`${SERVER_URL}/api/choicelist?choicelistType=customs_unit`)
      .then(res => {
        this.setState({
          TenHQPH: res.data,
          TenHQ: res.data
        });
      })
      .catch(err => console.log(err));

    axios
      .get(`${SERVER_URL}/api/choicelist?choicelistType=tax_type`)
      .then(res => {
        this.setState({
          SacThue: res.data
        });
      })
      .catch(err => console.log(err));

    axios
      .get(`${SERVER_URL}/api/choicelist?choicelistType=company_list`)
      .then(res => {
        this.setState({
          TenDVXNK: res.data
        });
      })
      .catch(err => console.log(err));

    axios
      .get(`${SERVER_URL}/api/choicelist?choicelistType=type_code`)
      .then(res => {
        this.setState({
          LHXNK: res.data
        });
      })
      .catch(err => console.log(err));

    axios
      .get(`${SERVER_URL}/api/choicelist?choicelistType=treasury_list`)
      .then(res => {
        this.setState({
          KhoBac: res.data
        });
      })
      .catch(err => console.log(err));
  }

  handleDismissAlert = () => {
    this.setState({ isShow: false });
  };

  handleChangeNgayLap = date => {
    this.setState({
      NGAY_LAP: date
    });
  };

  // Your custom transformation/validation
  myCustomFunction = rawValue => {
    const parsedDate = moment(rawValue, "DDMMYY");
    if (parsedDate.isValid()) {
      this.setState({ NGAY_HL: parsedDate });
    }
  };

  // handleKeyPressSoTK = e => {
  //   if (e.keyCode == 13 || e.keyCode == 9) {
  //     let SO_TK = e.target.value;
  //     query("get_payment_confirm_reject", this.props.curentUser, SO_TK)
  //       .then(res => {
  //         console.log(res);
  //         this.setState({
  //           KHCT: res.KYHIEU_CT,
  //           MA_CHUONG: res.MA_CHUONG,
  //           MA_DV: res.MA_DV,
  //           MA_HQ: res.MA_HQ,
  //           MA_KB: res.MA_KB,
  //           MA_LH: res.MA_LH,
  //           MA_NH_PH: res.MA_NH_PH_TH,
  //           // MA_ST: res.MA_ST,
  //           NGAY_CT: moment(res.NGAY_CT, "DD/MM/YYYY"),
  //           NGAY_HL: moment(res.NGAY_HL, "DD/MM/YYYY"),
  //           DUNO_TO: res.SOTIEN,
  //           SO_CT: res.SO_CT,
  //           SO_TK: res.SO_TK,
  //           MA_NH_PH: res.TAIKHOAN_TH,
  //           TEN_DV: res.TEN_DV,
  //           TEN_HQ: res.TEN_HQ,
  //           TEN_KB: res.TEN_KB,
  //           TEN_LH: res.TEN_LH,
  //           TEN_NH_PH: res.TEN_NH_PH_TH,
  //           TKKB: res.TKKB
  //         });
  //       })
  //       .catch(err => console.log(err));
  //   }
  // };

  handleClickAccept = e => {
    e.preventDefault();
    this.setState({ loading: true });
    const data = {
      SO_CT: this.state.SO_CT_NHAP,
      KYHIEU_CT: this.state.KHCT_NHAP,
      NGAY_CT: this.state.NGAY_CT,
      MA_DV: this.state.MA_DV,
      TEN_DV: this.state.TEN_DV,
      MA_CHUONG: this.state.MA_CHUONG,
      MA_HQ: this.state.MA_HQ,
      TEN_HQ: this.state.TEN_HQ,
      SO_TK: this.state.SO_TK,
      NGAY_DK: moment(this.state.NGAY_DK).format("DD/MM/YYYY"),
      MA_LH: this.state.MA_LH,
      TEN_LH: this.state.TEN_LH,
      MA_KB: this.state.MA_KB
    };

    console.log(data);

    // const socket = openSocket(SOCKET_URL);

    invoke(
      "create_new_voucher",
      this.props.currentUser,
      "tax_mission_confirm",
      JSON.stringify(data)
    )
      .then(res => {
        // console.log(res);
        if (res.code == 2) { // Catch error from server
          this.setState({
            loading: false,
            message: res.details.split(":")[2].replace(")", ""),
            isShow: true
          });
        } else {
          if (res[0].status === "SUCCESS") {
            this.setState({
              loading: false,
              message: "SUCCESS",
              isShow: true
            });
            this.props.socket.emit("customs-xacnhanhtnvt", { 
              message: "Hey, Haiquan have created XacNhanHTNVT successfully!",
              socketid: this.props.socket.id,
              user: localStorage.getItem("username"),
              role: localStorage.getItem("role"),
              unitid: this.state.MA_KB
             });
          } else {
            // console.log(res.details)
            this.setState({
              loading: false,
              message: "ERROR",
              isShow: true
            });
          }
        }
        
      })
      .catch(err => {
        console.log(err);
        this.setState({
          loading: false,
          message: "ERROR",
          isShow: true
        });
      });
  };

  handleClickReject = e => {
    e.preventDefault();
    // this.setState({ loading: true });
  };

  handleClickBack = () => {
    this.props.history.goBack();
  };

  handleDismissAlert = () => {
    this.setState({ isShow: false });
  };

  render() {
    let showAlert = null;
    let infoSummary = null;

    if (this.state.isShow) {
      if (this.state.message === "SUCCESS") {
        showAlert = (
          <Alert bsStyle="success">
            <strong>Chúc mừng!</strong> Bạn đã tạo mới dữ liệu thành công.
            <p className="dismiss-alert">
              <span onClick={this.handleDismissAlert}>x</span>
            </p>
          </Alert>
        );
      } else {
        showAlert = (
          <Alert bsStyle="danger">
            <strong>Lỗi!</strong> Xin vui lòng kiểm tra lại. {this.state.message}
            <p className="dismiss-alert">
              <span onClick={this.handleDismissAlert}>x</span>
            </p>
          </Alert>
        );
      }
    }

    if (!this.state.loading) {
      infoSummary = (
        <form>
          <h3>Xác Nhận HTNVT</h3>
          <div className="row">
            <Col sm={2}>
              <FormGroup>
                <span>Số TK</span>
                <FormControl
                  type="text"
                  value={this.state.SO_TK}
                  placeholder="Nhập"
                  onChange={e => this.setState({ SO_TK: e.target.value })}
                  onKeyDown={this.handleKeyPressSoTK}
                  disabled="true"
                />
              </FormGroup>
            </Col>
            <Col sm={2}>
              <span>Ngày ĐK</span>
              <DatePicker
                dateFormat="DD/MM/YYYY"
                selected={this.state.NGAY_DK}
                onChange={date => this.handleChangeNgayDK(date)}
                className="form-control"
                onChangeRaw={event => this.myCustomFunction(event.target.value)}
                disabled
              />
            </Col>
          </div>

          <div className="row">
            <Col sm={8}>
              <FormGroup>
                <span>Nơi mở tờ khai</span>
                <div className="row">
                  <Col sm={4}>
                    <FormControl
                      type="text"
                      value={this.state.MA_HQ}
                      disabled="true"
                    />
                  </Col>
                  <Col sm={8}>
                    <FormGroup controlId="formControlsSelect">
                      <FormControl
                        inputRef={el => (this.inputEl = el)}
                        componentClass="select"
                        placeholder="select"
                        value={this.state.MA_HQ}
                        onChange={this.handleChangeTENHQPH}
                        disabled="true"
                      >
                        <option value="">Chi cục HQ CK ...</option>
                        {this.state.TenHQPH.map(c => (
                          <option key={c.code} value={c.code}>
                            {c.name}
                          </option>
                        ))}
                      </FormControl>
                    </FormGroup>
                  </Col>
                </div>
              </FormGroup>
            </Col>
          </div>

          <div className="row">
            <Col sm={8}>
              <FormGroup>
                <span>Loại hình XNK</span>
                <div className="row">
                  <Col sm={4}>
                    <FormControl
                      type="text"
                      value={this.state.MA_LH}
                      disabled="true"
                    />
                  </Col>
                  <Col sm={8}>
                    <FormGroup controlId="formControlsSelect">
                      <FormControl
                        inputRef={el => (this.inputEl = el)}
                        componentClass="select"
                        placeholder="select"
                        value={this.state.MA_LH}
                        onChange={this.handleChangeTENHQPH}
                        disabled="true"
                      >
                        <option value="">Chi cục HQ CK ...</option>
                        {this.state.LHXNK.map(c => (
                          <option key={c.code} value={c.code}>
                            {c.name}
                          </option>
                        ))}
                      </FormControl>
                    </FormGroup>
                  </Col>
                </div>
              </FormGroup>
            </Col>
          </div>

          <div className="row">
            <Col sm={8}>
              <FormGroup>
                <span>Đơn vị xuất nhập khẩu</span>
                <div className="row">
                  <Col sm={4}>
                    <FormControl
                      type="text"
                      value={this.state.MA_DV}
                      disabled="true"
                    />
                  </Col>
                  <Col sm={8}>
                    <FormGroup controlId="formControlsSelect">
                      <FormControl
                        inputRef={el => (this.inputEl = el)}
                        componentClass="select"
                        placeholder="select"
                        value={this.state.MA_DV}
                        onChange={this.handleChangeTENHQPH}
                        disabled="true"
                      >
                        <option value="">Chi cục HQ CK ...</option>
                        {this.state.TenDVXNK.map(c => (
                          <option key={c.code} value={c.code}>
                            {c.name}
                          </option>
                        ))}
                      </FormControl>
                    </FormGroup>
                  </Col>
                </div>
              </FormGroup>
            </Col>
          </div>

          <div className="row">
            <Col sm={8}>
              <FormGroup>
                <span>Kho bạc</span>
                <div className="row">
                  <Col sm={4}>
                    <FormControl
                      type="text"
                      value={this.state.MA_KB}
                      placeholder="Nhập"
                      onChange={e => this.setState({ MA_KB: e.target.value })}
                      onKeyDown={this.handleKeyPressMaKB}
                      disabled="true"
                    />
                  </Col>
                  <Col sm={8}>
                    <FormGroup controlId="formControlsSelect">
                      <FormControl
                        inputRef={el => (this.inputEl = el)}
                        componentClass="select"
                        placeholder="select"
                        value={this.state.MA_KB}
                        onChange={this.handleChangeKhoBac}
                        disabled="true"
                      >
                        <option value="">...</option>
                        {this.state.KhoBac.map(c => (
                          <option key={c.code} value={c.code}>
                            {c.name}
                          </option>
                        ))}
                      </FormControl>
                    </FormGroup>
                  </Col>
                </div>
              </FormGroup>
            </Col>
            <Col sm={2}>
              <span>Tài khoản KB</span>
              <FormControl
                type="text"
                value={this.state.TKKB}
                disabled="true"
              />
            </Col>
          </div>

          <div className="row">
            <Col sm={3}>
              <span>Sắc thuế</span>
            </Col>
            <Col sm={3}>
              <span>Số tiền</span>
            </Col>
          </div>

          {this.state.ST.map(obj => {
            return (
              <div className="row">
                <Col sm={3}>
                  <FormGroup controlId="formControlsSelect">
                    <FormControl
                      inputRef={el => (this.inputEl = el)}
                      componentClass="select"
                      placeholder="select"
                      value={obj.ST_KEY}
                      disabled
                    >
                      <option value="">Chọn giá trị ...</option>
                      {this.state.SacThue.map(c => (
                        <option key={c.code} value={c.prefix}>
                          {c.name}
                        </option>
                      ))}
                    </FormControl>
                  </FormGroup>
                </Col>
                <Col sm={2}>
                  <NumberFormat
                    value={obj.ST_MONEY || 0}
                    thousandSeparator={true}
                    className="form-control money"
                    disabled
                  />
                </Col>
              </div>
            );
          })}

          <div className="row">
            <Col sm={3}>
              <span>Tổng</span>
            </Col>
            <Col sm={2}>
              <NumberFormat
                value={this.state.DUNO_TO || 0}
                thousandSeparator={true}
                className="form-control money"
                disabled
              />
            </Col>
          </div>

          <hr />

          <div className="row">
            <Col sm={2}>
              <span>Số chứng từ yc</span>
            </Col>
            <Col sm={2}>
              <FormControl
                type="text"
                value={this.state.SO_CT}
                disabled="true"
              />
            </Col>
            <Col sm={2}>
              <span>Ký hiệu chứng từ</span>
            </Col>
            <Col sm={2}>
              <FormControl
                type="text"
                value={this.state.KHCT}
                disabled="true"
              />
            </Col>
            <Col sm={2}>
              <span>Ngày lập</span>
            </Col>
            <Col sm={2}>
              <DatePicker
                dateFormat="DD/MM/YYYY"
                selected={this.state.NGAY_LAP}
                onChange={date => this.handleChangeNgayLap(date)}
                className="form-control"
                onChangeRaw={event => this.myCustomFunction(event.target.value)}
                disabled
              />
            </Col>
          </div>

          <div className="row">
            <Col sm={12}>
              <FormGroup>
                <span>Ngân hàng phát hành</span>
                <div className="row">
                  <Col sm={2}>
                    <FormControl
                      type="text"
                      value={this.state.MA_NH_PH}
                      disabled="true"
                    />
                  </Col>
                  <Col sm={6}>
                    {/* <FormGroup controlId="formControlsSelect"> */}
                    <FormControl
                      inputRef={el => (this.inputEl = el)}
                      componentClass="select"
                      placeholder="select"
                      value={this.state.MA_NH_PH}
                      onChange={this.handleChangeTENHQ}
                      disabled="true"
                    >
                      <option value="">...</option>
                      {this.state.TenNH.map(c => (
                        <option key={c.code} value={c.code}>
                          {c.name}
                        </option>
                      ))}
                      >
                    </FormControl>
                    {/* </FormGroup> */}
                  </Col>
                </div>
              </FormGroup>
            </Col>
          </div>

          <div className="row">
            <Col sm={12}>
              <FormGroup>
                <span>Ngân hàng thụ hưởng</span>
                <div className="row">
                  <Col sm={2}>
                    <FormControl
                      type="text"
                      value={this.state.MA_NH_TH}
                      disabled="true"
                    />
                  </Col>
                  <Col sm={6}>
                    {/* <FormGroup controlId="formControlsSelect"> */}
                    <FormControl
                      inputRef={el => (this.inputEl = el)}
                      componentClass="select"
                      placeholder="select"
                      value={this.state.MA_NH_TH}
                      onChange={this.handleChangeTENHQ}
                      disabled="true"
                    >
                      <option value="">...</option>
                      {this.state.TenNH.map(c => (
                        <option key={c.code} value={c.code}>
                          {c.name}
                        </option>
                      ))}
                      >
                    </FormControl>
                    {/* </FormGroup> */}
                  </Col>
                </div>
              </FormGroup>
            </Col>
          </div>

          <div className="row">
            <Col sm={4}>
              <span>Số tiền</span>
              <NumberFormat
                value={this.state.DUNO_TO || 0}
                thousandSeparator={true}
                className="form-control money"
                onValueChange={values => {
                  const { formattedValue, value, floatValue } = values;
                  this.setState({ DUNO_TO: floatValue || 0 });
                }}
                disabled
              />
            </Col>
          </div>

          {/* <hr /> */}

          <div className="row">
            <Col sm={2}>
              <FormGroup>
                <span>Số chứng từ</span>
                <FormControl
                  type="text"
                  value={this.state.SO_CT_NHAP}
                  placeholder="Nhập"
                  onChange={e => this.setState({ SO_CT_NHAP: e.target.value })}
                />
              </FormGroup>
            </Col>
            <Col sm={2}>
              <FormGroup>
                <span>Kí hiệu chứng từ</span>
                <FormControl
                  type="text"
                  value={this.state.KHCT_NHAP}
                  placeholder="Nhập"
                  onChange={e => this.setState({ KHCT_NHAP: e.target.value })}
                />
              </FormGroup>
            </Col>
            <Col sm={2}>
              <span>Ngày lập</span>
              <DatePicker
                dateFormat="DD/MM/YYYY"
                selected={this.state.NGAY_LAP}
                onChange={date => this.handleChangeNgayLap(date)}
                className="form-control"
                onChangeRaw={event => this.myCustomFunction(event.target.value)}
              />
            </Col>
          </div>

          {/* <hr /> */}

          <div className="ButtonGroup">
            <Button bsStyle="primary" onClick={this.handleClickAccept}>
              Chấp nhận
            </Button>
            <Button bsStyle="primary" onClick={this.handleClickReject}>
              Từ chối
            </Button>
            <Button bsStyle="primary" onClick={this.handleClickBack}>
              Thoát
            </Button>
          </div>
        </form>
      );
    } else {
      infoSummary = <Spinner />;
    }

    return (
      <div className="Declaration">
        {showAlert}
        {infoSummary}
      </div>
    );
  }
}

export default XacNhanHTVNT;
