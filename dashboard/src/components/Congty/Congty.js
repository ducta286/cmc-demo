import React, { Component } from 'react';
import './Congty.css';
import { Table } from 'react-bootstrap';
import Spinner from '../UI/Spinner/Spinner';
import { NavLink, Link } from 'react-router-dom';
import { Button, ButtonToolbar, Glyphicon } from 'react-bootstrap';
import { query } from "../../api";
import axios from "axios";
import { SERVER_URL, SOCKET_URL } from "../../api_server";
import Pagination from "react-js-pagination";

class Congty extends Component {

    state = {
        tokhais: [],
        loading: false,
        itemsCountPerPage: null,
        totalItemsCount: null,
        rowNumber: 0,
        customs: [],
        banks: [],
        companies: [],
        treasuries: [],
        activePage: 1
    };

    componentDidMount() {
        const { currentUser, currentUnit } = this.props;
        // console.log(currentUnit);
        this.setState({ loading: true });
        query("company_get_tax_declarations", currentUser, currentUnit,
            "0", (new Date(new Date().getTime()-(60*24*60*60*1000))).getTime(), (new Date).getTime(), "0")
            .then(res => {
            // console.log(res);
            this.setState({ 
                tokhais: res.data, 
                totalItemsCount: res.count, 
                itemsCountPerPage: res.limit,
                loading: false
             });
        });

        this.getParallel();
    }

    getParallel = async () => {
        const urls = [
          "/api/choicelist?choicelistType=customs_unit",
          "/api/choicelist?choicelistType=bank_list",
          "/api/choicelist?choicelistType=company_list",
          "/api/choicelist?choicelistType=treasury_list"
        ];
    
        try {
          const promises = urls.map(url =>
            axios.get(`${SERVER_URL}` + url).then(res => res.data)
          );
          await Promise.all(promises).then(results => {
            this.setState({
              customs: results[0],
              banks: results[1],
              companies: results[2],
              treasuries: results[3]
            });
          });
        } catch (err) {
          throw err;
        }
    }

    handlePageChange = (currentPage) => {
        // console.log(`active page is ${currentPage}`);
        this.setState({activePage: currentPage});
        const { currentUser, currentUnit } = this.props;
        query("company_get_tax_declarations", currentUser, currentUnit,
        "0", (new Date(new Date().getTime()-(60*24*60*60*1000))).getTime(), (new Date).getTime(), "" + currentPage)
            .then(res => {
                this.setState({ tokhais: res.data, loading: false });
            });
        this.setState({ rowNumber: (currentPage-1) * 5 });
    }

    createCompanyHandler = () => {
        this.props.history.push(`/base/declaration`);
    }

    render() {
        let companyInfo = null;
        
        let infoSummary = null;
        
        if(this.state.tokhais) {
            companyInfo = (
                this.state.tokhais.map( (obj, index) => {
                    let TenHQArray = this.state.customs.filter(hq => hq.code === obj.MA_HQ);
                    let TenDVArray = this.state.companies.filter(dv => dv.code === obj.MA_DV);
                    let TenNHArray = this.state.banks.filter(nh => nh.code === obj.MA_NH);
                    let TenKBArray = this.state.treasuries.filter(kb => kb.code === obj.MA_KB);
                    let TenHQ = null;
                    let TenDV = null;
                    let TenNH = null;
                    let TenKB = null;
                    if(TenHQArray.length > 0) {
                        TenHQ = TenHQArray[0].name;
                    }
                    if(TenDVArray.length > 0) {
                        TenDV = TenDVArray[0].name;
                    }
                    if(TenNHArray.length > 0) {
                        TenNH = TenNHArray[0].name;
                    }
                    if(TenKBArray.length > 0) {
                        TenKB = TenKBArray[0].name;
                    }

                    let status = null;
                    switch(obj.STATUS) {
                        case "1":
                            status = TenHQ + " tạo mới TBT/thông tin thuế của tờ khai";
                            break;
                        case "2":
                            status = TenDV +" đang yêu cầu thanh toán";
                            break;
                        case "3":
                            status = "Ngân hàng đã thanh toán, chờ xác nhận HTNVT";
                            break;
                        case "4":
                            status = TenDV + " đã hoàn thành NVT";
                            break;
                        case "5":
                            status = TenKB + " đã hoạch toán kho bạc";
                            break;
                        default:
                            break;
                    }

                    return (
                        <tr key={index + 1}>
                            <td>{index + 1 + this.state.rowNumber}</td>
                            <td>{obj.MA_HQ}</td>
                            <td>{TenHQ}</td>
                            <td>{obj.SO_TK}</td>
                            <td>{obj.NGAY_DK}</td>
                            <td>{status}</td>
                            <td>
                            {
                                    obj.STATUS === "1" ? (<Link to={{
                                        pathname: '/base/giaynopthue',
                                        search: '?id=' + obj.SO_TK,
                                    }}>Giấy Nộp Thuế</Link>) : null
                                }
                            </td>
                        </tr>
                        
                    );
                })
            );
        }

        if(!this.state.loading) {
            infoSummary = (
                        <Table striped bordered condensed hover responsive className="TableInfo">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Mã HQ</th>    
                                <th>Tên HQ</th>
                                <th>Số tờ khai</th>
                                <th>Ngày đăng ký</th>
                                <th>Tình trạng</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                                {companyInfo}
                            </tbody>
                        </Table>
            );
        } else {
            infoSummary = <Spinner />;
        }

        

        return (
            <div> 
                <div className="Info">
                    <h3>Danh Sách Tờ Khai Nộp Thuế</h3>
                    {infoSummary}
                    
                    {/*
                     TODO: if have total record from server
                     {
                        (this.state.tokhais.length > 5) ? (
                            <div className="d-flex flex-row py-4 align-items-center pagination-custom">
                                <Pagination
                                    totalRecords={100}
                                    pageLimit={4}
                                    pageNeighbours={1}
                                    onPageChanged={this.onPageChanged}
                                />
                            </div>) : null
                    } */}
                    <div className="d-flex flex-row align-items-center pagination-custom">
                        <Pagination
                            hideDisabled
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.itemsCountPerPage}
                            totalItemsCount={this.state.totalItemsCount}
                            pageRangeDisplayed={this.state.itemsCountPerPage}
                            onChange={this.handlePageChange}
                            className="pagination"
                            prevPageText='Prev'
                            nextPageText='Next'
                        />
                    </div>
                </div>
                {/* <ButtonToolbar className="ButtonToolBar">
                       <Button bsStyle="success" onClick={this.createCompanyHandler}><Glyphicon glyph="plus" />Tạo Mới</Button>
                </ButtonToolbar> */}
            </div>
        );
    }
}

export default Congty;