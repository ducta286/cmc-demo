import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row
} from "reactstrap";
import axios from "axios";
import { SERVER_URL } from "../../../api_server";
import { Redirect, Link } from "react-router-dom";
import { fakeAuth } from "../../../components/Common/FakeAuth";
import Spinner from "../../../components/UI/Spinner/Spinner";

class Login extends Component {
  state = {
    username: "",
    password: "",
    redirectToReferrer: false,
    alertMessage: "",
    loading: false
  };

  handleLogin = () => {
    const data = {
      username: this.state.username,
      password: this.state.password
    };

    this.setState({ loading: true });
    axios
      .post(`${SERVER_URL}/api/signin`, data)
      .then(res => {
        // console.log(res);
        if (res.data.code === 1) {
          localStorage.setItem("username", res.data.message[0].username);
          localStorage.setItem("unit", res.data.message[0].unit);
          localStorage.setItem("role", res.data.message[0].role);
          localStorage.setItem("isAuthenticated", true);
          fakeAuth.authenticate(() => {
            this.setState({ redirectToReferrer: true, loading: false });
          });
          // this.props.history.push("/");
        } else {
          this.setState({
            alertMessage: "Lỗi! " + res.data.message,
            loading: false
          });
        }
      })
      .catch(err => {
        console.log(err);
        this.setState({
          alertMessage: "Lỗi! " + err,
          loading: false
        });
      });
  };

  render() {
    const { from } = this.props.location.state || { from: { pathname: "/" } };
    const { redirectToReferrer } = this.state;

    if (redirectToReferrer) {
      return <Redirect to={from} />;
    }

    let renderView = null;

    if (!this.state.loading) {
      renderView = (
        <div className="app flex-row align-items-center">
          <Container>
          
            <Row className="justify-content-center">
              <Col md="8">
                <CardGroup>
                  <Card className="p-4">
                  <form>
                    <CardBody>
                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="text"
                          placeholder="Username"
                          value={this.state.username}
                          onChange={e =>
                            this.setState({ username: e.target.value })}
                        />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="password"
                          placeholder="Password"
                          value={this.state.password}
                          onChange={e =>
                            this.setState({ password: e.target.value })}
                        />
                      </InputGroup>
                      <Row>
                        <Col xs="6">
                          <Button
                            color="primary"
                            className="px-4"
                            onClick={this.handleLogin}
                          >
                            Login
                          </Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          {/* <Button color="link" className="px-0"> */}
                            <Link to="/register">Register?</Link>
                          {/* </Button> */}
                        </Col>
                      </Row>
                      <span style={{ color: "red" }}>
                        {this.state.alertMessage}
                      </span>
                    </CardBody>
                    </form>
                  </Card>
                  {/* <Card
                  className="text-white bg-primary py-5 d-md-down-none"
                  style={{ width: 44 + "%" }}
                >
                  <CardBody className="text-center">
                    <div>
                      <h2>Sign up</h2>
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua.
                      </p>
                      <Button color="primary" className="mt-3" active>
                        Register Now!
                      </Button>
                    </div>
                  </CardBody>
                </Card> */}
                </CardGroup>
              </Col>
            </Row>
            
          </Container>
        </div>
      );
    } else {
      renderView = <Spinner />;
    }

    return <div>{renderView}</div>;
  }
}

export default Login;
