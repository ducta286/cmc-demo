import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import MultiStep from '../Step/MultiStep'
// import CreateDeclaration from './CreateDeclaration';
// import RequiredPayment from './RequiredPayment';
// import ConfirmPayment from './ConfirmPayment';
import CreateDeclaration from '../../../components/Declaration/Declaration';
import RequiredPayment from '../../../components/Declaration/GiayNopThue';
import ConfirmPayment from '../../../components/Declaration/XacNhanNopThue';

require ('../Step/Steps.css');

class Declaration extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.sampleStore = {
      email: '',
      gender: '',
      savedToCloud: false
    };
  }

  componentDidMount() {}

  componentWillUnmount() {}

  getStore() {
    return this.sampleStore;
  }

  updateStore(update) {
    this.sampleStore = {
      ...this.sampleStore,
      ...update,
    }
  }

  render() {
    const steps =
  [
    {name: 'Tạo tờ khai thuế', component: <CreateDeclaration/>},
    {name: 'Yêu cầu thanh toán', component: <RequiredPayment/>},
    {name: 'Xác nhận thanh toán', component: <ConfirmPayment/>}
  ]
    return (
     <div>
        <div>
          <MultiStep steps={steps}/>
        </div>
      </div>
    )
  }
}

export default Declaration;
