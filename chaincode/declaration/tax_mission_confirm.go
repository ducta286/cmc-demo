package main

import (
	"encoding/json"
	"errors"
	"strconv"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

// define a tax mission confirm voucher, create by customs role
type TaxMissionConfirm struct {
	DUYET		string	`json:"DUYET"`			//approve
	SO_CT		string	`json:"SO_CT"`
	KYHIEU_CT	string	`json:"KYHIEU_CT"`
	NGAY_CT		string	`json:"NGAY_CT"`
	MA_DV		string	`json:"MA_DV"`
	TEN_DV		string	`json:"TEN_DV"`
	MA_CHUONG	string	`json:"MA_CHUONG"`
	MA_HQ		string	`json:"MA_HQ"`
	TEN_HQ		string	`json:"TEN_HQ"`
	SO_TK		string	`json:"SO_TK"`
	NGAY_DK		string	`json:"NGAY_DK"`
	MA_LH		string	`json:"MA_LH"`
	TEN_LH		string	`json:"TEN_LH"`
	VOUCHER_TYPE string	`json:VOUCHER_TYPE`
	CREATED_DATE string	`json:CREATED_DATE`
	STATUS		string	`json:STATUS`
	MA_KB		string	`json:MA_KB`
}

func (t *SimpleChaincode) create_new_tax_mission_confirm(stub shim.ChaincodeStubInterface, params string) ([]byte, error) {
	var taxMC TaxMissionConfirm
	var err = json.Unmarshal([]byte(params), &taxMC)
	if err != nil {
		logger.Infof("create_new_voucher: Corrupt tax mission confirm record: %s, got error: %s", params, err)
		return nil, errors.New(CORRUPT_STRUCTURE)
	}
	declarationNumber := taxMC.SO_TK
	if declarationNumber == "" {
		logger.Errorf("[create_new_voucher]: Invalid declarationNumber provided: %v", declarationNumber)
		return nil, errors.New(INVALID_DECLARATION_NUMBER)
	}
	voucherNumber := taxMC.SO_CT
	if voucherNumber == "" {
		logger.Errorf("[create_new_voucher]: Invalid voucherNumber provided: %v", voucherNumber)
		return nil, errors.New(INVALID_VOUCHER_NUMBER)
	}
	record, err := stub.GetState(voucherNumber)
	if record != nil {
		logger.Errorf("tax mission confirm already exists %v", voucherNumber)
		return nil, errors.New(KEY_STATE_EXIST)
	}
	taxMC.VOUCHER_TYPE = TAX_MISSION_CONFIRM
	taxMC.CREATED_DATE = strconv.FormatInt(time.Now().UnixNano() / int64(time.Millisecond), 10)
	taxMC.STATUS = CREATE_NEW
	_, err = t.tax_mission_confirm_save_changes(stub, taxMC)
	if err != nil {
		logger.Errorf("[create_new_voucher]: Error saving changes: %s", err)
		return nil, errors.New(SAVE_ERROR)
	}
	return []byte(CREATED_SUCCESS), nil
}

// save tax mission confirm voucher after created
func (t *SimpleChaincode) tax_mission_confirm_save_changes(stub shim.ChaincodeStubInterface, taxMC TaxMissionConfirm) (bool, error) {

	bytes, err := json.Marshal(taxMC)

	if err != nil {
		logger.Infof("SAVE_CHANGES: Error converting tax mission confirm record: %s", err)
		return false, errors.New(CONVERTING_ERROR)
	}

	err = stub.PutState(taxMC.SO_CT, bytes)

	if err != nil {
		logger.Infof("SAVE_CHANGES: Error storing tax mission confirm record: %s", err)
		return false, errors.New(STORING_ERROR)
	} else {
		status := APPROVE
		// if taxMC.DUYET == "false" {
		// 	status = REJECT
		// }
		_, err = t.update_payment_confirm_reject_status(stub, taxMC.SO_TK, status)
		if err == nil {
			logger.Infof("UPDATE_STATUS: Successfuly update status payment confirm reject")
		} else {
			logger.Infof("UPDATE_STATUS: Failed update status payment confirm reject")
		}

		_, err = t.update_tax_declaration_status(stub, taxMC.SO_TK, CUSTOMS_TAX_MISSION, taxMC.CREATED_DATE)
		if err == nil {
			logger.Infof("UPDATE_STATUS: Successfuly update status tax declaration")
		} else {
			logger.Infof("UPDATE_STATUS: Failed update status tax declaration")
		}
	}

	logger.Infof("SAVE_CHANGES: Successfuly storing tax mission confirm record")
	return true, nil
}

// treasury get list tax mission confirm
func (t *SimpleChaincode) treasury_get_tax_mission_confirm(stub shim.ChaincodeStubInterface, treasuryCode string, fromDate string, toDate string, page string) ([]byte, error) {
	
	var queryString = "{\"selector\":{\"$and\":["+
		"{\"MA_KB\":{\"$eq\":\""+treasuryCode+"\"}},"+
		"{\"VOUCHER_TYPE\":{\"$eq\":\""+TAX_MISSION_CONFIRM+"\"}},"+
		"{\"CREATED_DATE\":{\"$gte\":\""+fromDate+"\"}},{\"CREATED_DATE\":{\"$lte\":\""+toDate+"\"}}]}"
	if INDEX_SUPPORTED {
		queryString += ",\"sort\":[{\"CREATED_DATE\":\"desc\"}],\"use_index\":[\"_design/payment_request\", \"payment_request-index\"]"
	}
	queryString += "}"
	
	result, err := t.get_query_result_array(stub, queryString, page)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (t *SimpleChaincode) update_tax_mission_confirm_status(stub shim.ChaincodeStubInterface, declarationNumber string, status string) (bool, error){
	
	var taxMC TaxMissionConfirm

	bytes, err := t.get_voucher_info(stub, TAX_MISSION_CONFIRM, declarationNumber)

	if err != nil {
		logger.Infof("[update_tax_mission_confirm]: Error retrieving tax mission confirm: %s, with declarationNumber = %v", err, declarationNumber)
		return false, errors.New("Error retrieving tax mission confirm with declarationNumber = " + declarationNumber)
	}

	err = json.Unmarshal(bytes, &taxMC)
	if err != nil {
		logger.Infof("RETRIEVE_ITEM: Corrupt payment confirm reject record: %s, got error: %s", bytes, err)
		return false, errors.New("RETRIEVE_ITEM: Corrupt payment confirm reject record" + string(bytes))
	} else {
		taxMC.STATUS = status
		_, err = t.tax_mission_confirm_save_changes(stub, taxMC)
		if err != nil {
			logger.Infof("[update_tax_mission_confirm]: Error saving changes: %s", err)
			return false, errors.New("Error saving changes")
		}
		return true, nil
	}
}