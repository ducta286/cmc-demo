sudo ln -sfn ../loyalty/vendor/ $PWD/vendor
chaincodeID=$(ps -ax | awk '$0~/\.\/declaration/{print $1}')
if [ ! -z "$chaincodeID" ];then  
  kill $chaincodeID
fi

echo $PWD

while getopts "a:v:" opt; do
  case "$opt" in
    a)  ADDRESS=$OPTARG
    ;;
    v)  VERSION=$OPTARG
    ;;
    *) 
      echo "Unknown $opt"
      exit 1
    ;;
  esac
done

: ${VERSION:=1.7}
: ${ADDRESS:=localhost:7052}

# chmod u+x ./contract

# sleep 1
echo "go build -i && CORE_PEER_ADDRESS=$ADDRESS CORE_PEER_LOCALMSPID=Org1MSP CORE_VM_ENDPOINT=unix:///var/run/docker.sock CORE_CHAINCODE_ID_NAME=declaration:$VERSION ./declaration"
go build -i && CORE_PEER_ADDRESS=$ADDRESS CORE_PEER_LOCALMSPID=Org1MSP CORE_VM_ENDPOINT=unix:///var/run/docker.sock CORE_CHAINCODE_ID_NAME=declaration:$VERSION ./declaration
