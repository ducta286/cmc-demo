package main

import (
	"encoding/json"
	"errors"
	"strconv"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

// define a payment request voucher, create by company role
type PaymentRequest struct {
	SO_CT			string 	`json:"SO_CT"`			//voucherNumber
	KYHIEU_CT		string 	`json:"KYHIEU_CT"`		//voucherPrefix
	NGAY_CT			string 	`json:"NGAY_CT"`		//voucherDate
	MA_DV			string 	`json:"MA_DV"`			//unitCode
	TEN_DV			string 	`json:"TEN_DV"`			//unitName
	MA_CHUONG		string 	`json:"MA_CHUONG"`		//chapterCode
	MA_HQ			string 	`json:"MA_HQ"`			//customsCode
	TEN_HQ			string 	`json:"TEN_HQ"`			//customsName
	SO_TK			string 	`json:"SO_TK"`			//declarationNumber
	NGAY_DK			string 	`json:"NGAY_DK"`		//registerDate
	MA_LH			string 	`json:"MA_LH"`			//typeCode
	TEN_LH			string 	`json:"TEN_LH"`			//typeName
	MA_KB			string 	`json:"MA_KB"`			//treasuryCode
	TEN_KB			string 	`json:"TEN_KB"`			//treasuryName
	TKKB			string 	`json:"TKKB"`			//treasuryAccount
	SOTIEN_TO		float32	`json:"SOTIEN_TO"`		//moneyNumberSum
	MA_ST			string 	`json:"MA_ST"`			//taxType
	SOTIEN			float32	`json:"SOTIEN"`			//moneyNumber
	MA_NH_PH		string 	`json:"MA_NH_PH"`		//bankIssueCode
	TEN_NH_PH		string 	`json:"TEN_NH_PH"`		//bankIssueName
	TAIKHOAN_PH		string 	`json:"TAIKHOAN_PH"`	//accountIssueCode
	TEN_TAIKHOAN_PH	string 	`json:"TEN_TAIKHOAN_PH"`//accountIssueName
	VOUCHER_TYPE	string	`json:VOUCHER_TYPE`		//
	CREATED_DATE	string	`json:CREATED_DATE`		// indexing
	STATUS			string	`json:STATUS`			// 0-new create, 1-approve, 2-reject
}

func (t *SimpleChaincode) create_new_payment_request(stub shim.ChaincodeStubInterface, params string) ([]byte, error) {
	var payReq PaymentRequest
	var err = json.Unmarshal([]byte(params), &payReq)
	if err != nil {
		logger.Infof("create_new_voucher: Corrupt payment request record: %s, got error: %s", params, err)
		return nil, errors.New(CORRUPT_STRUCTURE)
	}
	declarationNumber := payReq.SO_TK
	if declarationNumber == "" {
		logger.Errorf("[create_new_voucher]: Invalid declarationNumber provided: %v", declarationNumber)
		return nil, errors.New(INVALID_DECLARATION_NUMBER)
	}
	voucherNumber := payReq.SO_CT
	if voucherNumber == "" {
		logger.Errorf("[create_new_voucher]: Invalid voucherNumber provided: %v", voucherNumber)
		return nil, errors.New(INVALID_VOUCHER_NUMBER)
	}
	record, err := stub.GetState(voucherNumber)
	if record != nil {
		logger.Errorf("Payment request already exists %v", voucherNumber)
		return nil, errors.New(KEY_STATE_EXIST)
	}
	payReq.STATUS = CREATE_NEW
	payReq.VOUCHER_TYPE = PAYMENT_REQUEST
	payReq.CREATED_DATE = strconv.FormatInt(time.Now().UnixNano() / int64(time.Millisecond), 10)
	_, err = t.payment_request_save_changes(stub, payReq)
	if err != nil {
		logger.Errorf("[create_new_voucher]: Error saving changes: %s", err)
		return nil, errors.New(SAVE_ERROR)
	}
	return []byte(CREATED_SUCCESS), nil
}

// save payment request voucher after created
func (t *SimpleChaincode) payment_request_save_changes(stub shim.ChaincodeStubInterface, payReq PaymentRequest) (bool, error) {

	bytes, err := json.Marshal(payReq)

	if err != nil {
		logger.Infof("SAVE_CHANGES: Error converting payment request record: %s", err)
		return false, errors.New(CONVERTING_ERROR)
	}

	err = stub.PutState(payReq.SO_CT, bytes)

	if err != nil {
		logger.Infof("SAVE_CHANGES: Error storing payment request record: %s", err)
		return false, errors.New(STORING_ERROR)
	} else {
		_, err = t.update_tax_declaration_status(stub, payReq.SO_TK, COMPANY_PAYMENT_REQUEST, payReq.CREATED_DATE)
		if err == nil {
			logger.Infof("UPDATE_STATUS: Successfuly update status tax declaration")
		} else {
			logger.Infof("UPDATE_STATUS: Failed update status tax declaration")
		}
	}

	logger.Infof("SAVE_CHANGES: Successfuly storing payment request record")
	return true, nil
}

// bank get payment request list of them
func (t *SimpleChaincode) bank_get_payment_requests(stub shim.ChaincodeStubInterface, bankCode string, fromDate string, toDate string, page string) ([]byte, error) {
	
	var queryString = "{\"selector\":{\"$and\":["+
		"{\"MA_NH_PH\":{\"$eq\":\""+bankCode+"\"}},"+
		"{\"VOUCHER_TYPE\":{\"$eq\":\""+PAYMENT_REQUEST+"\"}},"+
		"{\"CREATED_DATE\":{\"$gte\":\""+fromDate+"\"}},{\"CREATED_DATE\":{\"$lte\":\""+toDate+"\"}}]}"
		//",{\"STATUS\":{\"$eq\":\"0\"}}"+
	if INDEX_SUPPORTED {
		queryString += ",\"sort\":[{\"CREATED_DATE\":\"desc\"}],\"use_index\":[\"_design/payment_request\", \"payment_request-index\"]"
	}
	queryString += "}"
	
	result, err := t.get_query_result_array(stub, queryString, page)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (t *SimpleChaincode) update_payment_request_status(stub shim.ChaincodeStubInterface, declarationNumber string, status string) (bool, error){
	
	var paymentRequest PaymentRequest

	bytes, err := t.get_voucher_info(stub, PAYMENT_REQUEST, declarationNumber)

	if err != nil {
		logger.Infof("[update_payment_request]: Error retrieving payment request: %s, with declarationNumber = %v", err, declarationNumber)
		return false, errors.New("Error retrieving payment request with declarationNumber = " + declarationNumber)
	}

	err = json.Unmarshal(bytes, &paymentRequest)
	if err != nil {
		logger.Infof("RETRIEVE_ITEM: Corrupt payment request record: %s, got error: %s", bytes, err)
		return false, errors.New("RETRIEVE_ITEM: Corrupt payment request record" + string(bytes))
	} else {
		paymentRequest.STATUS = status
		_, err = t.payment_request_save_changes(stub, paymentRequest)
		if err != nil {
			logger.Infof("[update_payment_request]: Error saving changes: %s", err)
			return false, errors.New("Error saving changes")
		}
		return true, nil
	}
}

func (t *SimpleChaincode) bank_get_company_code(stub shim.ChaincodeStubInterface, bankCode string, declarationNumber string) (string, error) {
	
	var queryString = "{\"selector\":{\"$and\":["+
		"{\"MA_NH_PH\":{\"$eq\":\""+bankCode+"\"}},"+
		"{\"SO_TK\":{\"$eq\":\""+declarationNumber+"\"}},"+
		"{\"VOUCHER_TYPE\":{\"$eq\":\""+PAYMENT_REQUEST+"\"}}]}}"

	result, err := t.get_query_result_object(stub, queryString)
	if err != nil {
		return "", err
	}

	var paymentRequest PaymentRequest
	err = json.Unmarshal(result, &paymentRequest)
	if err != nil {
		return "", err
	}
	companyCode := paymentRequest.MA_DV
	return companyCode, nil
}