chaincodeID=$(ps -ax | awk '$0~/\.\/corporate/{print $1}')
if [ ! -z "$chaincodeID" ];then  
  kill $chaincodeID
fi

ln -sfn $PWD $GOPATH/src
ln -sfn ../loyalty/vendor/ $PWD/vendor

while getopts "a:v:" opt; do
  case "$opt" in
    a)  ADDRESS=$OPTARG
    ;;
    v)  VERSION=$OPTARG
    ;;
    *) 
      echo "Unknown $opt"
      exit 1
    ;;
  esac
done

: ${VERSION:=1.0}
: ${ADDRESS:=localhost:7052}

# chmod u+x ./corporate

# sleep 1
echo "go build -i && CORE_PEER_ADDRESS=$ADDRESS CORE_PEER_LOCALMSPID=Org1MSP CORE_VM_ENDPOINT=unix:///var/run/docker.sock CORE_CHAINCODE_ID_NAME=corporate:$VERSION ./corporate"
go build -i && CORE_PEER_ADDRESS=$ADDRESS CORE_PEER_LOCALMSPID=Org1MSP CORE_VM_ENDPOINT=unix:///var/run/docker.sock CORE_CHAINCODE_ID_NAME=corporate:$VERSION ./corporate
