import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import Transportation from './containers/Transportation/Transportation';
import Declaration from './components/Declaration/Declaration';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="container">
          <Declaration />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
