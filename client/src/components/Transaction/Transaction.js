import React, { Component } from 'react';
import './Transaction.css';
import Spinner from '../UI/Spinner/Spinner';
import { Form, Col, FormGroup, FormControl, ControlLabel, Button, ButtonToolbar } from 'react-bootstrap';
import { invoke } from "../../api";

class Transaction extends Component {
    state = {
        SO_HD: '',
        MA_NGAN_HANG: 0,
        NGAY_THANH_TOAN: '',
        BEN_NHAN: '',
        LAN_THANH_TOAN: 0,
        SO_TIEN: 0,
        submitted: false,
        loading: false
    };

    orderHandler = (event) => {
        event.preventDefault();
        this.setState( { loading: true } );
        const data = {
            SO_HD: this.state.SO_HD,
            MA_NGAN_HANG: parseInt(this.state.MA_NGAN_HANG),
            NGAY_THANH_TOAN: this.state.NGAY_THANH_TOAN,
            BEN_NHAN: this.state.BEN_NHAN,
            LAN_THANH_TOAN: parseInt(this.state.LAN_THANH_TOAN),
            SO_TIEN: parseInt(this.state.SO_TIEN),
        };
        invoke("poc_create_transaction", JSON.stringify(data)).then(data => {
            this.setState({ loading: false });
            this.props.history.push(`/contract`);
        });
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        this.setState({ SO_HD: id });
    }

    render() {  
        let submittedOrder = null;

        if(this.state.loading) {
            submittedOrder = <Spinner />;
        } else {
            submittedOrder = (
                <div className="NewCompany">
                    <h1>Tạo Mới Giao Dịch</h1>
                    <Form horizontal onSubmit={this.orderHandler} className="FormCompany">
                        <FormGroup controlId="formHorizontalSO_HD">
                            <Col componentClass={ControlLabel} sm={2}>
                                Số HĐ:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" disabled="true" value={this.state.SO_HD} placeholder="Enter SO_HD" 
                                onChange={ (event) => this.setState({SO_HD: event.target.value}) } />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalMA_NGAN_HANG">
                            <Col componentClass={ControlLabel} sm={2}>
                                Mã Ngân Hàng:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.MA_NGAN_HANG} placeholder="Enter MA_NGAN_HANG" 
                                onChange={ (event) => this.setState({MA_NGAN_HANG: event.target.value}) } />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalNGAY_THANH_TOAN">
                            <Col componentClass={ControlLabel} sm={2}>
                                Ngày Thanh Toán:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.NGAY_THANH_TOAN} placeholder="Enter NGAY_THANH_TOAN" 
                                onChange={ (event) => this.setState({NGAY_THANH_TOAN: event.target.value}) } />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalBEN_NHAN">
                            <Col componentClass={ControlLabel} sm={2}>
                                Bên Nhận:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.BEN_NHAN} placeholder="Enter BEN_NHAN" 
                                onChange={ (event) => this.setState({BEN_NHAN: event.target.value}) } />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalLAN_THANH_TOAN">
                            <Col componentClass={ControlLabel} sm={2}>
                                Lần Thanh Toán:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.LAN_THANH_TOAN} placeholder="Enter LAN_THANH_TOAN" 
                                onChange={ (event) => this.setState({LAN_THANH_TOAN: event.target.value}) } />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalSO_TIEN">
                            <Col componentClass={ControlLabel} sm={2}>
                                Số Tiền:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.SO_TIEN} placeholder="Enter SO_TIEN" 
                                onChange={ (event) => this.setState({SO_TIEN: event.target.value}) } />
                            </Col>
                        </FormGroup>
                        
                        <ButtonToolbar className="ButtonToolBar">
                            <Button bsStyle="primary" onClick={this.orderHandler}>Tạo Mới</Button>
                        </ButtonToolbar>
                    </Form>
                </div>
            );
        }

        return (
            <div>
                {submittedOrder}
            </div>
        );
    }
}

export default Transaction;