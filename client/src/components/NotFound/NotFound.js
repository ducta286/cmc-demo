import React from 'react';

const NotFound = () => {
    return (
        <div className="text-center">
            <h1>Địa chỉ web không tồn tại!!!</h1>
        </div>
    );
};

export default NotFound;