import React, { Component } from 'react';
import './Company.css';
import { Table } from 'react-bootstrap';
import Spinner from '../UI/Spinner/Spinner';
import { NavLink } from 'react-router-dom';
import { Button, ButtonToolbar, Glyphicon } from 'react-bootstrap';
import { query } from "../../api";

class Company extends Component {

    state = {
        companies: [],
        loading: false
    };

    componentDidMount() {
        this.setState({ loading: true });
        query("poc_get_all_assets", "corporate").then(companies => {
            this.setState({ companies: companies, loading: false});
        });
    }

    createCompanyHandler = () => {
        this.props.history.push(`/company/new`);
    }

    render() {
        // const errorText = (
        //     <tr>
        //         <td colSpan="4" style={{ color: 'red' }}>Không lấy được dữ liệu!!!</td>
        //     </tr>
        // );
        let companyInfo = null;
        
        let infoSummary = null;

        if(this.state.companies) {
            companyInfo = (
                this.state.companies.map( (obj, index) => {
                    return (
                        <tr key={index + 1}>
                            <td>{index + 1}</td>
                            <td>{obj.MA_SO_THUE}</td>
                            <td>{obj.TEN_DN}</td>
                            <td>{obj.MA_NGAN_HANG}</td>
                            <td>{obj.TEN_NGAN_HANG}</td>
                            <td>{obj.SO_TAI_KHOAN}</td>
                            <td>{obj.TEN_TAI_KHOAN}</td>
                            <td>
                                <NavLink to={'/company/update/${obj.MA_SO_THUE}'} exact activeClassName="active" activeStyle={{ color: 'blue' }}><Glyphicon glyph="edit" style={{ color: 'red' }} /></NavLink>
                                <NavLink to={'/company/history/${obj.MA_SO_THUE}'} exact activeClassName="active" 
                                    activeStyle={{ color: 'blue' }}><Glyphicon glyph="bitcoin" style={{ color: 'yellow' }} /></NavLink>
                            </td>
                        </tr>
                        
                    );
                })
            );
        }

        if(!this.state.loading) {
            infoSummary = (
                
                        <Table striped bordered condensed hover responsive className="TableInfo">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>MA_SO_THUE</th>
                                <th>TEN_DN</th>
                                <th>MA_NGAN_HANG</th>
                                <th>TEN_NGAN_HANG</th>
                                <th>SO_TAI_KHOAN</th>
                                <th>TEN_TAI_KHOAN</th>
                                <th>ACTIONS</th>
                            </tr>
                            </thead>
                            <tbody>
                                {companyInfo}
                            </tbody>
                        </Table>
                    
            );
        } else {
            infoSummary = <Spinner />;
        }

        

        return (
            <div> 
                <div className="Info">
                    <h1>Danh Sách Doanh Nghiệp</h1>
                    {infoSummary}
                </div>
                <ButtonToolbar className="ButtonToolBar">
                       <Button bsStyle="success" onClick={this.createCompanyHandler}><Glyphicon glyph="plus" />Tạo Mới</Button>
                </ButtonToolbar>
            </div>
        );
    }
}

export default Company;