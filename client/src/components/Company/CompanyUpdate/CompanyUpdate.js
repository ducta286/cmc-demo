import React, { Component } from 'react';
import './CompanyUpdate.css';
import Spinner from '../../UI/Spinner/Spinner';
import { Form, Col, FormGroup, FormControl, ControlLabel, Button, ButtonToolbar } from 'react-bootstrap';
import { invoke, query } from "../../../api";

class CompanyUpdate extends Component {
    state = {
        taxId: 0,
        companyName: '',
        bankId: 0,
        bankName: '',
        accountNumb: 0,
        accountName: '',
        submitted: false,
        loading: false
    };

    orderHandler = (event) => {
        event.preventDefault();
        this.setState( { loading: true } );
        const id = this.props.match.params.id;
        const data = {
            TEN_DN: this.state.companyName,
            MA_NGAN_HANG: parseInt(this.state.bankId),
            TEN_NGAN_HANG: this.state.bankName,
            SO_TAI_KHOAN: parseInt(this.state.accountNumb),
            TEN_TAI_KHOAN: this.state.accountName
        };
        // console.log(data);
        invoke("poc_update_corporate", id, JSON.stringify(data)).then(data => {
            this.setState({ loading: false });
            this.props.history.push(`/company`);
        });
    }

    componentDidMount() {
        this.setState({ loading: true });
        // console.log(this.props);
        const id = this.props.match.params.id;
        query("poc_get_corporate_details", id).then(data => {
            const resData = data;
            this.setState({ 
                taxId: resData.MA_SO_THUE,
                companyName: resData.TEN_DN,
                bankId: resData.MA_NGAN_HANG,
                bankName: resData.TEN_NGAN_HANG,
                accountNumb: resData.SO_TAI_KHOAN,
                accountName: resData.TEN_TAI_KHOAN,
                loading: false 
            });
        });
        // axios.get('http://localhost:3000/company/' + id)
        // .then(res => {
        //     // console.log(res.data);
        //     const resData = res.data;
        //     this.setState({ 
        //         taxId: resData.MA_SO_THUE,
        //         companyName: resData.TEN_DN,
        //         bankId: resData.MA_NGAN_HANG,
        //         bankName: resData.TEN_NGAN_HANG,
        //         accountNumb: resData.SO_TAI_KHOAN,
        //         accountName: resData.TEN_TAI_KHOAN,
        //         loading: false 
        //     });
        // })
        // .catch( err => {
        //     console.log(err);
        //     // this.setState( { loading: false } );
        // } );
    }

    render() {  
        let submittedOrder = null;

        if(this.state.loading) {
            submittedOrder = <Spinner />;
        } else {
            submittedOrder = (
                <div className="NewCompany">
                    <h1>Cập Nhật Doanh Nghiệp</h1>
                    <Form horizontal onSubmit={this.orderHandler} className="FormCompany">
                        <FormGroup controlId="formHorizontalTaxID">
                            <Col componentClass={ControlLabel} sm={2}>
                                Mã Số Thuế:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" disabled="true" value={this.state.taxId} placeholder="Enter TaxID" 
                                onChange={ (event) => this.setState({taxId: event.target.value}) } />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalCompanyName">
                            <Col componentClass={ControlLabel} sm={2}>
                                Tên Công Ty:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.companyName} placeholder="Enter CompanyName" 
                                onChange={ (event) => this.setState({companyName: event.target.value}) } />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalBankID">
                            <Col componentClass={ControlLabel} sm={2}>
                                Mã Ngân Hàng:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.bankId} placeholder="Enter BankID" 
                                onChange={ (event) => this.setState({bankId: event.target.value}) } />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalBankName">
                            <Col componentClass={ControlLabel} sm={2}>
                                Tên Ngân Hàng:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.bankName} placeholder="Enter BankName" 
                                onChange={ (event) => this.setState({bankName: event.target.value}) } />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalAccountNumb">
                            <Col componentClass={ControlLabel} sm={2}>
                                Số Tài Khoản:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.accountNumb} placeholder="Enter Account Number" 
                                onChange={ (event) => this.setState({accountNumb: event.target.value}) } />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalAccountName">
                            <Col componentClass={ControlLabel} sm={2}>
                                Tên Tài Khoản:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.accountName} placeholder="Enter AccountName" 
                                onChange={ (event) => this.setState({accountName: event.target.value}) } />
                            </Col>
                        </FormGroup>
                        
                        <ButtonToolbar className="ButtonToolBar">
                            <Button bsStyle="primary" onClick={this.orderHandler}>Cập Nhật</Button>
                        </ButtonToolbar>
                    </Form>
                </div>
            );
        }

        return (
            <div>
                {submittedOrder}
            </div>
        );
    }
}

export default CompanyUpdate;