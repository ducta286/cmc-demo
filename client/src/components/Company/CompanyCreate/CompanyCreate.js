import React, { Component } from 'react';
import './CompanyCreate.css';
import Spinner from '../../UI/Spinner/Spinner';
import { Form, Col, FormGroup, FormControl, ControlLabel, Button, ButtonToolbar } from 'react-bootstrap';
import { invoke } from "../../../api";

class CompanyCreate extends Component {
    state = {
        taxId: 0,
        companyName: '',
        bankId: 0,
        bankName: '',
        accountNumb: 0,
        accountName: '',
        submitted: false,
        loading: false
    };

    orderHandler = (event) => {
        event.preventDefault();
        this.setState( { loading: true } );
        const data = {
            MA_SO_THUE: parseInt(this.state.taxId),
            TEN_DN: this.state.companyName,
            MA_NGAN_HANG: parseInt(this.state.bankId),
            TEN_NGAN_HANG: this.state.bankName,
            SO_TAI_KHOAN: parseInt(this.state.accountNumb),
            TEN_TAI_KHOAN: this.state.accountName
        };
        // console.log(order);
        // axios.post('http://localhost:3000/new-company', data)
        //     .then(res => {
        //         console.log(res);
        //         this.setState({ loading: false });
        //     })
        //     .catch( err => {
        //         console.log(err);
        //         // this.setState( { loading: false } );
        //     } );
        invoke("poc_create_corporate", JSON.stringify(data)).then(data => {
            this.setState({ loading: false });
            this.props.history.push(`/company`);
        });
    }

    render() {  
        let submittedOrder = null;

        if(this.state.loading) {
            submittedOrder = <Spinner />;
        } else {
            submittedOrder = (
                <div className="NewCompany">
                    <h1>Tạo Mới Doanh Nghiệp</h1>
                    <Form horizontal onSubmit={this.orderHandler} className="FormCompany">
                        <FormGroup controlId="formHorizontalTaxID">
                            <Col componentClass={ControlLabel} sm={2}>
                                Mã Số Thuế:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.taxId} placeholder="Enter TaxID" 
                                onChange={ (event) => this.setState({taxId: event.target.value}) } />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalCompanyName">
                            <Col componentClass={ControlLabel} sm={2}>
                                Tên Công Ty:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.companyName} placeholder="Enter CompanyName" 
                                onChange={ (event) => this.setState({companyName: event.target.value}) } />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalBankID">
                            <Col componentClass={ControlLabel} sm={2}>
                                Mã Ngân Hàng:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.bankId} placeholder="Enter BankID" 
                                onChange={ (event) => this.setState({bankId: event.target.value}) } />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalBankName">
                            <Col componentClass={ControlLabel} sm={2}>
                                Tên Ngân Hàng:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.bankName} placeholder="Enter BankName" 
                                onChange={ (event) => this.setState({bankName: event.target.value}) } />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalAccountNumb">
                            <Col componentClass={ControlLabel} sm={2}>
                                Số Tài Khoản:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.accountNumb} placeholder="Enter Account Number" 
                                onChange={ (event) => this.setState({accountNumb: event.target.value}) } />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalAccountName">
                            <Col componentClass={ControlLabel} sm={2}>
                                Tên Tài Khoản:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.accountName} placeholder="Enter AccountName" 
                                onChange={ (event) => this.setState({accountName: event.target.value}) } />
                            </Col>
                        </FormGroup>
                        
                        <ButtonToolbar className="ButtonToolBar">
                            <Button bsStyle="primary" onClick={this.orderHandler}>Tạo Mới</Button>
                        </ButtonToolbar>
                    </Form>
                </div>
            );
        }

        return (
            <div>
                {submittedOrder}
            </div>
        );
    }
}

export default CompanyCreate;