import React, { Component } from 'react';
import './ContractHistory.css';
import { Table } from 'react-bootstrap';
import Spinner from '../../UI/Spinner/Spinner';
import { query } from "../../../api";

class ContractHistory extends Component {

    state = {
        history: [],
        loading: false
    };

    componentDidMount() {
        this.setState({ loading: true });
        const id = this.props.match.params.id;
        query("get_history", "contract", id, "10").then(history => {
            this.setState({ history: history, loading: false});
        });
    }

    render() {
        // const errorText = (
        //     <tr>
        //         <td colSpan="4" style={{ color: 'red' }}>Không lấy được dữ liệu!!!</td>
        //     </tr>
        // );
        let orderInfo = null;
        
        let infoSummary = null;

        if(this.state.history) {
            orderInfo = (
                this.state.history.map( (obj, index) => {
                    return (
                        <tr key={index + 1}>
                            <td>{index + 1}</td>
                            <td>{obj.Value.SO_HD}</td>
                            <td>{obj.Value.NGAY_HD}</td>
                            <td>{obj.Value.NGAY_TAO}</td>
                            <td>{obj.Value.MST_BEN_A}</td>
                            <td>{obj.Value.MST_BEN_B}</td>
                            <td>{obj.Value.SO_TIEN}</td>
                            <td>{obj.Value.VAT}</td>
                            <td>{obj.Value.TONG_TIEN}</td>
                            {/* <td>{obj.Value.REF}</td> */}
                            <td>{obj.Value.NGUOI_TAO}</td>
                            <td>{obj.Value.TRANG_THAI}</td>
                            <td>{obj.Timestamp}</td>
                        </tr>
                    );
                })
            );
        }

        if(!this.state.loading) {
            infoSummary = (
                <div className="Info">
                    <h1>Thông Tin Giao Dịch Hợp Đồng</h1>
                    <Table striped bordered condensed hover responsive className="TableInfo">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>SO_HD</th>
                            <th>NGAY_HD</th>
                            <th>NGAY_TAO</th>
                            <th>MST_BEN_A</th>
                            <th>MST_BEN_B</th>
                            <th>SO_TIEN</th>
                            <th>VAT</th>
                            <th>TONG_TIEN</th>
                            {/* <th>REF</th> */}
                            <th>NGUOI_TAO</th>
                            <th>TRANG_THAI</th>
                            <th>Timestamp</th>
                        </tr>
                        </thead>
                        <tbody>
                            {orderInfo}
                        </tbody>
                    </Table>
                </div>
            );
        } else {
            infoSummary = <Spinner />;
        }

        

        return (
            <div>
                {infoSummary}
            </div>
        );
    }
}

export default ContractHistory;