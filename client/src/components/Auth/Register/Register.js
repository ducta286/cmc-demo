import React, { Component } from 'react';
import './Register.css';
import axios from 'axios';
import { SERVER_URL } from '../../../api_server';

class Register extends Component {
    state = {
        username: '',
        password: '',
        email: ''
    };

    submitRegisterHandler = (event) => {
        event.preventDefault();
        const data = {
            username: this.state.username,
            password: this.state.password,
            email: this.state.email
        };
        axios.post(`${SERVER_URL}/auth/register`, data)
            .then(res => {
                //console.log(res);
                this.props.history.push('/login');
            })
            .catch(err => {
                console.log(err);
            });
    }

    render() {
        return (
            <div className="login-page">
                <div className="form">
                <form className="login-form" onSubmit={this.submitRegisterHandler}>
                    <input type="text" 
                        value={this.state.username} 
                        onChange={(e) => this.setState({username: e.target.value})} 
                        placeholder="username"/>
                    <input type="password" 
                        value={this.state.password} 
                        onChange={(e) => this.setState({password: e.target.value})} 
                        placeholder="password"/>
                    <input type="email" 
                        value={this.state.email} 
                        onChange={(e) => this.setState({email: e.target.value})} 
                        placeholder="email"/>
                    <button>register</button>
                </form>
                </div>
            </div>
        );
    }
}

export default Register;